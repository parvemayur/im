package com.Intranet.Controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.Intranet.BO.ConferenceMasterBO;
import com.Intranet.JavaBean.ConferenceMasterBean;

/**
 * Servlet implementation class ConferenceMaster
 */
public class ConferenceMaster extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ConferenceMaster() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session=request.getSession();
		ConferenceMasterBean  bean=new ConferenceMasterBean();
		bean.setConf_master_name(request.getParameter("conf_master_name"));
		bean.setConf_master_plant(request.getParameter("conf_master_plant"));
		bean.setConf_master_descr(request.getParameter("conf_master_descr"));
		bean.setConf_master_seatcount(Integer.parseInt(request.getParameter("conf_master_seatcount")));
		bean.setConf_master_status(Integer.parseInt(request.getParameter("conf_master_status")));
		
		ConferenceMasterBO bo=new ConferenceMasterBO();
		int result=bo.addConferenceMaster(bean);
		
		System.out.print(result);
		if(result==1)
		{
			session.setAttribute("alert", "Conference master created...");
			response.sendRedirect("conference_master.jsp");
		}
		else if(result==0)
		{
			session.setAttribute("alert", "Conference master already exists...");
			response.sendRedirect("conference_master.jsp");
		}
		else
		{
			session.setAttribute("alert", "Problem creating Conference master...");
			response.sendRedirect("conference_master.jsp");
		}
	}

}
