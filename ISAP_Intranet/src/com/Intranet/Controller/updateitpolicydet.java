package com.Intranet.Controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.Intranet.BO.updatehrdetBO;
import com.Intranet.BO.updateitpolicydetBO;
import com.Intranet.JavaBean.updatehrdetBean;
import com.Intranet.JavaBean.updateitpolicydetBean;

/**
 * Servlet implementation class updateitpolicydet
 */
public class updateitpolicydet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public updateitpolicydet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        PrintWriter out = response.getWriter();
        HttpSession session=request.getSession();
        try {

        updateitpolicydetBean bean=new updateitpolicydetBean(request);
        updateitpolicydetBO cbo=new updateitpolicydetBO();
        boolean flag=cbo.updateitpolicy(bean);
          if (flag == true) {

                System.out.println("in if controller");

                response.sendRedirect("dashboard.jsp?msg=" + "Succesfull");
            } else {

                response.sendRedirect("it_updates.jsp?msg" + "Failed");
                //response.sendRedirect("adminlogin.jsp");
                //RequestDispatcher rd = getServletContext().getRequestDispatcher("/adminlogin.jsp");
                //RequestDispatcher rd = request.getRequestDispatcher("/index.jsp");
                //rd.forward(request, response);
            }

                
        
        } finally {            
            out.close();
        }

		

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
