package com.Intranet.Controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.Intranet.BO.ConferenceBookingBO;
import com.Intranet.BO.ConferenceMasterBO;
import com.Intranet.JavaBean.ConferenceBookingBean;
import com.Intranet.JavaBean.ConferenceMasterBean;

/**
 * Servlet implementation class ConferenceMaster
 */
public class ConferenceBooking extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ConferenceBooking() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		ConferenceBookingBean bean = new ConferenceBookingBean();
		bean.setConf_booking_timestart(request.getParameter("conf_date") + " "
				+ request.getParameter("conf_booking_timestart"));
		bean.setConf_booking_timeend(request.getParameter("conf_date") + " "
				+ request.getParameter("conf_booking_timeend"));
		bean.setConf_master_id(Integer.parseInt(request
				.getParameter("conf_master_id")));
		bean.setConf_booking_status(Integer.parseInt(request
				.getParameter("conf_booking_status")));
		bean.setConf_booking_type(request
				.getParameter("conf_booking_type"));
		bean.setEmp_master_id(Integer.parseInt(""+session.getAttribute("userid")));

		ConferenceBookingBO bo = new ConferenceBookingBO();
		int result = bo.addConferenceBooking(bean);

		System.out.print(result);
		if (result == 1) {
			session.setAttribute("alert", "Conference room booked...");
			response.sendRedirect("conference_booking.jsp");
		} else if (result == 0) {
			session.setAttribute("alert",
					"Time slot mentioned already booked...");
			response.sendRedirect("conference_booking.jsp");
		} else {
			session.setAttribute("alert", "Problem bookinh Conference room...");
			response.sendRedirect("conference_booking.jsp");
		}
	}

}
