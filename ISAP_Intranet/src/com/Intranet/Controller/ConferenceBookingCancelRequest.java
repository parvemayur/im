package com.Intranet.Controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.Intranet.BO.ConferenceBookingCancelRequestBO;
import com.Intranet.JavaBean.ConferenceBookingCancelRequestBean;
import com.Intranet.Util.Emailing;

/**
 * Servlet implementation class ConferenceBookingCancelRequest
 */
public class ConferenceBookingCancelRequest extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ConferenceBookingCancelRequest() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session=request.getSession();
		ConferenceBookingCancelRequestBean bean = new ConferenceBookingCancelRequestBean();
		bean.setConf_booking_cancel_request_to_employee_id(Integer.parseInt(request.getParameter("conf_booking_cancel_request_to_employee_id")));
		bean.setConf_booking_cancel_request_from_employee_id(Integer.parseInt(""+session.getAttribute("userid")));
		bean.setConf_booking_cancel_request_to_employee_email(request.getParameter("conf_booking_cancel_request_to_employee_email"));
		bean.setConf_booking_cancel_request_to_employee_name(request.getParameter("conf_booking_cancel_request_to_employee_name"));
		bean.setConf_booking_cancel_request_booking_id(Integer.parseInt(request.getParameter("conf_booking_cancel_request_booking_id")));
		bean.setConf_booking_cancel_request_descr(request.getParameter("conf_booking_cancel_request_descr"));
		bean.setConf_booking_cancel_request_status(0);
		
		ConferenceBookingCancelRequestBO bo=new ConferenceBookingCancelRequestBO();
		int result=bo.requestCancelBooking(bean);
		
		if (result == 1) {
			session.setAttribute("alert", "Reschedule request sent...");
			try
			{
				String message="Dear "+bean.getConf_booking_cancel_request_to_employee_name()+"<br/><br/>";
				message+=session.getAttribute("username")+" has requested to reschedule conference.<br/>";
				message+="For more details please login to portal.";
				
				Emailing.sendmail(bean.getConf_booking_cancel_request_to_employee_name(), bean.getConf_booking_cancel_request_to_employee_email(), "Conference reschedule notification", message);
			}
			catch(Exception e)
			{
				
			}
			response.sendRedirect("conference_booking.jsp");
		}else {
			session.setAttribute("alert", "Problem occured...");
			response.sendRedirect("conference_booking.jsp");
		}
	}

}
