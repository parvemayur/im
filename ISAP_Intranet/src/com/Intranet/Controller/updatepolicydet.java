package com.Intranet.Controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.Intranet.BO.updatepolicydetBO;
import com.Intranet.JavaBean.updatepolicydetBean;

/**
 * Servlet implementation class updatepolicydet
 */
public class updatepolicydet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public updatepolicydet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

      
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

        PrintWriter out = response.getWriter();
        HttpSession session=request.getSession();
        try {

        updatepolicydetBean bean=new updatepolicydetBean(request);
        updatepolicydetBO cbo=new updatepolicydetBO();
        boolean flag=cbo.updatepolicy(bean);
          if (flag == true) {

                System.out.println("in if controller");

                response.sendRedirect("dashboard.jsp?msg=" + "Succesfull");
            } else {

                response.sendRedirect("hr_policies.jsp?msg" + "Failed");
                //response.sendRedirect("adminlogin.jsp");
                //RequestDispatcher rd = getServletContext().getRequestDispatcher("/adminlogin.jsp");
                //RequestDispatcher rd = request.getRequestDispatcher("/index.jsp");
                //rd.forward(request, response);
            }

                
        
        } finally {            
            out.close();
        }
	}

}
