package com.Intranet.Controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.Intranet.BO.updatechairmanmsgBO;
import com.Intranet.BO.updatehrdetBO;
import com.Intranet.JavaBean.updatechairmanmsgBean;
import com.Intranet.JavaBean.updatehrdetBean;

/**
 * Servlet implementation class updatechairmanmsg
 */
public class updatechairmanmsg extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public updatechairmanmsg() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		 PrintWriter out = response.getWriter();
	        HttpSession session=request.getSession();
	        try {

	        updatechairmanmsgBean bean=new updatechairmanmsgBean(request);
	        updatechairmanmsgBO cbo=new updatechairmanmsgBO();
	        boolean flag=cbo.updatechairmanmsg(bean);
	          if (flag == true) {

	                System.out.println("in if controller");

	                response.sendRedirect("chairmans_msg.jsp?msg=" + "Succesfull");
	            } else {

	                response.sendRedirect("chairmans_msg.jsp?msg" + "Failed");
	                //response.sendRedirect("adminlogin.jsp");
	                //RequestDispatcher rd = getServletContext().getRequestDispatcher("/adminlogin.jsp");
	                //RequestDispatcher rd = request.getRequestDispatcher("/index.jsp");
	                //rd.forward(request, response);
	            }

	                
	        
	        } finally {            
	            out.close();
	        }

			

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
