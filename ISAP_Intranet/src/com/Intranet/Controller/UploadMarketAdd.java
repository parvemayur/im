package com.Intranet.Controller;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.mail.Session;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.Intranet.Util.doConnection;

/**
 * Servlet implementation class UploadMarketAdd
 */
public class UploadMarketAdd extends HttpServlet {
	private static final long serialVersionUID = 1L;
	final static String image_resource_path = "/res/images";
	// private final String UPLOAD_DIRECTORY = "C:/uploads";
	private static final String UPLOAD_DIRECTORY = "upload";
	private static final int THRESHOLD_SIZE = 1024 * 1024 * 3; // 3MB
	private static final int MAX_FILE_SIZE = 1024 * 1024 * 40; // 40MB
	//private static final int MAX_REQUEST_SIZE = 1024 * 1024 * 50; // 50MB
	
	
	///////
	
	 private static final String DATA_DIRECTORY = "data";
	    private static final int MAX_MEMORY_SIZE = 1024 * 1024 * 2;
	    private static final int MAX_REQUEST_SIZE = 1024 * 1024;


	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UploadMarketAdd() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		
		String adtitle = "";
		String category = "";
		String price = "";
		String addescrip = "";
		String login_id = "";
		File uploadedFile;
		String dirPath = "c:\\uploads";
		String fileNameWithExt = null;
		FileItem item = null;
		boolean flag = false;
		String path=null;
		File storeFile=null;

		// checks if the request actually contains upload file
		if (!ServletFileUpload.isMultipartContent(request)) {
			PrintWriter writer = response.getWriter();
			writer.println("Request does not contain upload data");
			writer.flush();
			return;
		}
		DiskFileItemFactory factory = new DiskFileItemFactory();
		factory.setSizeThreshold(THRESHOLD_SIZE);
		factory.setRepository(new File(System.getProperty("java.io.tmpdir")));

		ServletFileUpload upload = new ServletFileUpload(factory);
		upload.setFileSizeMax(MAX_FILE_SIZE);
		upload.setSizeMax(MAX_REQUEST_SIZE);

		// constructs the directory path to store upload file
		String uploadPath = System.getProperty("catalina.base")
				+ File.separator + UPLOAD_DIRECTORY;
		
		
		

	
		System.out.println("uploadPath=="+uploadPath);
		// creates the directory if it does not exist
		File uploadDir = new File(uploadPath);
		if (!uploadDir.exists()) {
			uploadDir.mkdir();
		}
		
		/////////////////////////////////////////
		
		
        
        System.out.println("tomcat path===="+System.getProperty("catalina.base"));
        
        ///////////////////////////////////////////////////


		List formItems = null;
		try {
			formItems = upload.parseRequest(request);

			Iterator iter = formItems.iterator();

			// iterates over form's fields
			while (iter.hasNext()) {
				 item = (FileItem) iter.next();
				// processes only fields that are not form fields
				if (!item.isFormField()) {
					String fileName = new File(item.getName()).getName();

					// saves the file on disk

					
				}else {

					if (item.getFieldName().equals("adtitle")) {
						adtitle = item.getString();

						// date1=(Date)sdf1.parse(item.getString("rdate"));

					}

					if (item.getFieldName().equals("txtcat")) {
						category = item.getString();

						// date1=(Date)sdf1.parse(item.getString("rdate"));

					}
					if (item.getFieldName().equals("txtprice")) {
						price = item.getString();
						// date1=(Date)sdf1.parse(item.getString("rdate"));

					}
					if (item.getFieldName().equals("addescrip")) {
						addescrip = item.getString();

						// date1=(Date)sdf1.parse(item.getString("rdate"));

					}
					if (item.getFieldName().equals("LoginID")) {
						login_id = item.getString();
						System.out.println("title=" + login_id);
						// date1=(Date)sdf1.parse(item.getString("rdate"));

					}

				}
			}} catch (FileUploadException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			System.out.println("outside loop");
			PreparedStatement ps = null;
			ResultSet rs = null;
			Connection con = null;
			// Date date=(date) date.getDate();
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			DateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy");
			Date date1 = new Date();
			Date date2 = new Date();
			System.out.println(dateFormat.format(date1));
			String date11 = dateFormat.format(date2);
			System.out.println("date11=" + date11);

			con = doConnection.getConnection();

			PreparedStatement ps1 = null;
			ResultSet rs1 = null;
			// ps1=con.prepareStatement("select pan from employeemaster where ");

			String fileName = item.getName();
			if (fileName.lastIndexOf(".") != -1 && fileName.lastIndexOf(".") != 0)
				System.out.println("extension==="
						+ fileName.substring(fileName.lastIndexOf(".") + 1));

			String filePath = uploadPath + File.separator+"\\"+login_id+"_"+date11+ "."+fileName.substring(fileName.lastIndexOf(".") + 1); 
		 storeFile = new File(filePath);
		
			path = uploadPath + "\\" + login_id +"_" + date11
					+ "."+fileName.substring(fileName.lastIndexOf(".") + 1);
			System.out.println("path==="
					+ path);

			try {
				item.write(storeFile);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			try {
				ps = con.prepareStatement("insert into ecommerce_advt (ecommerce_title,ecommerce_category,ecommerce_price,ecommerce_addescription,ecommerce_image1,ecommerce_empid,ecommerce_status,ecommerce_advtdate) values(?,?,?,?,?,?,?,?)");
				System.out.println("ps=" + ps);
				ps.setString(1, adtitle);
				ps.setString(2, category);
				ps.setInt(3, Integer.parseInt(price));
				ps.setString(4, addescrip);
				ps.setString(5,path+"");
				ps.setString(6, login_id);
				ps.setString(7, "Not Published");
				ps.setString(8, date11);
				System.out.println("ps=" + ps);
				ps.executeUpdate();
				flag = true;
				if (flag == true) {
					response.sendRedirect("ecommerce_product_list.jsp?msg="
							+ "Product is added sucessfully");
				} else {

					response.sendRedirect("market_advert.jsp?msg="
							+ "Operation Failed");
				}
				// System.out.println(filePart.getName());
				// System.out.println(filePart.getSize());
				// System.out.println(filePart.getContentType());

				// obtains input stream of the upload file
				// inputStream = filePart.getInputStream();

			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}


				
		}	
				
				

	}


