package com.Intranet.Controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class IndexController
 */
public class IndexController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public IndexController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session=request.getSession();
		if(request.getParameter("password").equals("1"))
		{
			session.setAttribute("userid", "010001");
			session.setAttribute("username", "Mr. Vijay B.Pusalkar");
			session.setAttribute("useremail", "parvemayur@gmail.com");
			session.setAttribute("usermobile", "8600173856");
		}
		else if(request.getParameter("password").equals("2"))
		{
			session.setAttribute("userid", "010002");
			session.setAttribute("username", "MRS.T.V.PUSALKAR");
			session.setAttribute("useremail", "gaurav1hargude@gmail.com");
			session.setAttribute("usermobile", "8600173856");
		}
		else if(request.getParameter("password").equals("3"))
		{
			session.setAttribute("userid", "010005");
			session.setAttribute("username", "Mr. Bhikaji M. Kalel");
			session.setAttribute("useremail", "langhepradnya@gmail.com");
			session.setAttribute("usermobile", "8600173856");
		}
		response.sendRedirect("dashboard.jsp");
	}

	

}
