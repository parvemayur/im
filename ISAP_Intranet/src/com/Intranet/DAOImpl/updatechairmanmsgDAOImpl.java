package com.Intranet.DAOImpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.Intranet.DAO.updatechairmanmsgDAO;
import com.Intranet.JavaBean.updatechairmanmsgBean;
import com.Intranet.Util.doConnection;

public class updatechairmanmsgDAOImpl implements updatechairmanmsgDAO {

	@Override
	public boolean updatecmsg(updatechairmanmsgBean bean) {
		boolean flag=false;
		
		
		
	    Connection con = null;
	    PreparedStatement ps = null;
	    ResultSet rs = null;
	    int cnt = 0;
	    System.out.println("in daoimpl");
	    try {
	        con = doConnection.getConnection();
	        ps = con.prepareStatement("insert into chairman_msg (chairman_msg_msgname,chairman_msg_year,chairman_msg_status) values (?,?,?)");

	        System.out.println("ps===" + ps);
	        
	        ps.setString(1, bean.getCmsg());
	        ps.setString(2, bean.getCyear());

	        ps.setString(3, bean.getCstatus());
	       	        System.out.println("ps===" + ps);
	        ps.executeUpdate();


	        flag = true;


	    } catch (Exception e) {
	        e.printStackTrace();
	    } finally {
	        try {
	            if (ps != null) {
	                ps.close();
	            }

	            if (con != null) {
	                con.close();
	            }

	            if (rs != null) {
	                rs.close();
	            }
	        } catch (SQLException se) {
	            se.printStackTrace();
	        }
	    }


	
		return flag;
	}

}
