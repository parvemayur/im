package com.Intranet.DAOImpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.Intranet.DAO.ConferenceBookingCancelRequestDAO;
import com.Intranet.JavaBean.ConferenceBookingCancelRequestBean;
import com.Intranet.Util.doConnection;

public class ConferenceBookingCancelRequestDAOImpl implements ConferenceBookingCancelRequestDAO{

	@Override
	public int requestCancelBooking(ConferenceBookingCancelRequestBean bean) {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
				Connection con = null;
				PreparedStatement ps = null;
				int result = -1;
				try {
					con = doConnection.getConnection();
					ps = con.prepareStatement("insert into conf_booking_cancel_request (conf_booking_cancel_request_booking_id,conf_booking_cancel_request_to_employee_id,conf_booking_cancel_request_from_employee_id,conf_booking_cancel_request_descr,conf_booking_cancel_request_status) values (?,?,?,?,?)");

					ps.setInt(1, bean.getConf_booking_cancel_request_booking_id());
					ps.setInt(2, bean.getConf_booking_cancel_request_to_employee_id());

					ps.setInt(3, bean.getConf_booking_cancel_request_from_employee_id());
					ps.setString(4, bean.getConf_booking_cancel_request_descr());
					ps.setInt(5, bean.getConf_booking_cancel_request_status());
					result = ps.executeUpdate();
				} catch (Exception e) {
					result = 0;
					
				} finally {
					try {
						if (ps != null) {
							ps.close();
						}

						if (con != null) {
							con.close();
						}
					} catch (SQLException se) {
						se.printStackTrace();
					}
				}
				return result;
	}

}
