package com.Intranet.DAOImpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.Intranet.DAO.updateitpolicydetDAO;
import com.Intranet.JavaBean.updateitpolicydetBean;
import com.Intranet.Util.doConnection;

public class updateitpolicydetDAOImpl implements updateitpolicydetDAO{

	@Override
	public boolean processquery(updateitpolicydetBean bean) {
boolean flag=false;
		
		
		
	    Connection con = null;
	    PreparedStatement ps = null;
	    ResultSet rs = null;
	    int cnt = 0;
	    System.out.println("in daoimpl");
	    try {
	        con = doConnection.getConnection();
	        ps = con.prepareStatement("insert into hr_updates (it_policies_name,it_policies_descr,it_policies_sortorder,it_policies_validfrom,it_policies_validto,it_policies_status) values (?,?,?,?,?,?)");

	        System.out.println("ps===" + ps);
	        
	        ps.setString(1, bean.getItptitle());
	        ps.setString(2, bean.getItpdetails());
	        

	        ps.setString(3, bean.getSortorder());
	        ps.setString(4, bean.getValidpfrm());
	        ps.setString(5, bean.getValidpto());
	        ps.setString(6, bean.getStatus());
	        System.out.println("ps===" + ps);
	        ps.executeUpdate();


	        flag = true;


	    } catch (Exception e) {
	        e.printStackTrace();
	    } finally {
	        try {
	            if (ps != null) {
	                ps.close();
	            }

	            if (con != null) {
	                con.close();
	            }

	            if (rs != null) {
	                rs.close();
	            }
	        } catch (SQLException se) {
	            se.printStackTrace();
	        }
	    }



		
		return flag;

	}
	

}
