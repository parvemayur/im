package com.Intranet.DAOImpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.Intranet.DAO.updatepolicydetDAO;
import com.Intranet.JavaBean.updatehrdetBean;
import com.Intranet.JavaBean.updatepolicydetBean;
import com.Intranet.Util.doConnection;

public class updatepolicydetDAOImpl implements updatepolicydetDAO {
	


@Override
public boolean processupdate(updatepolicydetBean bean) {
	boolean flag=false;
	
	
	
    Connection con = null;
    PreparedStatement ps = null;
    ResultSet rs = null;
    int cnt = 0;
    System.out.println("in daoimpl");
    try {
        con = doConnection.getConnection();
        ps = con.prepareStatement("insert into hr_policy (hr_policy_name,hr_policy_details,hr_policy_validfrom,hr_policy_validto,hr_policy_sort,hr_policy_status) values (?,?,?,?,?,?)");

        System.out.println("ps===" + ps);
        
        ps.setString(1, bean.getPtitle());
        ps.setString(2, bean.getPdetails());

        ps.setString(3, bean.getValidpfrm());
        ps.setString(4, bean.getValidpto());
        ps.setString(5, bean.getSortorder());
        ps.setString(6, bean.getStatus());
        System.out.println("ps===" + ps);
        ps.executeUpdate();


        flag = true;


    } catch (Exception e) {
        e.printStackTrace();
    } finally {
        try {
            if (ps != null) {
                ps.close();
            }

            if (con != null) {
                con.close();
            }

            if (rs != null) {
                rs.close();
            }
        } catch (SQLException se) {
            se.printStackTrace();
        }
    }


	return flag;
}

@Override
public Boolean proceed(updatehrdetBean bean) {
	// TODO Auto-generated method stub
	return null;
}


}
