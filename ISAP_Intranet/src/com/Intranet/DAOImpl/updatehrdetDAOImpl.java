package com.Intranet.DAOImpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.Intranet.DAO.updatehrdetDAO;
import com.Intranet.JavaBean.updatehrdetBean;
import com.Intranet.Util.doConnection;

public class updatehrdetDAOImpl implements updatehrdetDAO {

	@Override
	public Boolean proceed(updatehrdetBean bean) {

		boolean flag=false;
		
		
		
	    Connection con = null;
	    PreparedStatement ps = null;
	    ResultSet rs = null;
	    int cnt = 0;
	    System.out.println("in daoimpl");
	    try {
	        con = doConnection.getConnection();
	        ps = con.prepareStatement("insert into hr_updates (hr_update_name,hr_update_details,hr_update_sort,hr_update_validfrom,hr_update_validto,hr_update_status) values (?,?,?,?,?,?)");

	        System.out.println("ps===" + ps);
	        
	        ps.setString(1, bean.getPtitle());
	        ps.setString(2, bean.getPdetails());

	        ps.setString(3, bean.getSortorder());
	        ps.setString(4, bean.getValidpfrm());
	        ps.setString(5, bean.getValidpto());
	        ps.setString(6, bean.getStatus());
	        System.out.println("ps===" + ps);
	        ps.executeUpdate();


	        flag = true;


	    } catch (Exception e) {
	        e.printStackTrace();
	    } finally {
	        try {
	            if (ps != null) {
	                ps.close();
	            }

	            if (con != null) {
	                con.close();
	            }

	            if (rs != null) {
	                rs.close();
	            }
	        } catch (SQLException se) {
	            se.printStackTrace();
	        }
	    }



		
		return flag;
	}
	

}
