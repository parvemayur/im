package com.Intranet.DAOImpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.Intranet.DAO.ConferenceMasterDAO;
import com.Intranet.JavaBean.ConferenceMasterBean;
import com.Intranet.Util.doConnection;

public class ConferenceMasterDAOImpl implements ConferenceMasterDAO {

	@Override
	public int addConferenceMaster(ConferenceMasterBean bean) {
		// TODO Auto-generated method stub
		Connection con = null;
		PreparedStatement ps = null;
		int result = -1;
		try {
			con = doConnection.getConnection();
			ps = con.prepareStatement("insert into conference_master (conf_master_name,conf_master_plant,conf_master_seatcount,conf_master_descr,conf_master_status) values (?,?,?,?,?)");

			ps.setString(1, bean.getConf_master_name());
			ps.setString(2, bean.getConf_master_plant());

			ps.setInt(3, bean.getConf_master_seatcount());
			ps.setString(4, bean.getConf_master_descr());
			ps.setInt(5, bean.getConf_master_status());
			result = ps.executeUpdate();
		} catch (Exception e) {
			result = 0;
		} finally {
			try {
				if (ps != null) {
					ps.close();
				}

				if (con != null) {
					con.close();
				}
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		return result;
	}

}
