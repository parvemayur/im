package com.Intranet.DAOImpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.Intranet.DAO.ConferenceBookingDAO;
import com.Intranet.JavaBean.ConferenceBookingBean;
import com.Intranet.Util.doConnection;

public class ConferenceBookingDAOImpl implements ConferenceBookingDAO {

	@Override
	public int addConferenceBooking(ConferenceBookingBean bean) {
		// TODO Auto-generated method stub
		Connection con = null;
		PreparedStatement ps = null;
		int result = -1;
		try {
			con = doConnection.getConnection();
			ps = con.prepareStatement("select count(*) from conference_booking where ((? between conf_booking_timestart and conf_booking_timeend) or (? between conf_booking_timestart and conf_booking_timeend)) and conf_master_id=?");

			ps.setString(1, bean.getConf_booking_timestart());
			ps.setString(2, bean.getConf_booking_timeend());
			ps.setInt(3, bean.getConf_master_id());
			ResultSet rs = ps.executeQuery();
			if (rs.next() && rs.getInt(1)==0) {

				ps.close();
				ps = con.prepareStatement("insert into conference_booking (conf_master_id,emp_master_id,conf_booking_timestart,conf_booking_timeend,conf_booking_type,conf_booking_status) values (?,?,?,?,?,?)");

				ps.setInt(1, bean.getConf_master_id());
				ps.setInt(2, bean.getEmp_master_id());
				ps.setString(3, bean.getConf_booking_timestart());
				ps.setString(4, bean.getConf_booking_timeend());
				ps.setString(5, bean.getConf_booking_type());
				ps.setInt(6, bean.getConf_booking_status());
				result = ps.executeUpdate();
			} else {
				result = 0;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (ps != null) {
					ps.close();
				}

				if (con != null) {
					con.close();
				}
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		return result;
	}

}
