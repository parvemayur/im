package com.Intranet.Util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class doConnection {
	public static Connection getConnection() {
		Connection con = null;
		try {
			// the sql server driver string
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

			// the sql server url
//			String url = "jdbc:sqlserver://49.248.9.219:1433;DatabaseName=isap_intranet";
			String url = "jdbc:sqlserver://localhost:1433;DatabaseName=isap_intranet";
            con = DriverManager.getConnection(url,"sa","sa123");
		} catch (SQLException ex) {
			Logger.getLogger(doConnection.class.getName()).log(Level.SEVERE,
					null, ex);
		} catch (ClassNotFoundException ex) {
			Logger.getLogger(doConnection.class.getName()).log(Level.SEVERE,
					null, ex);
		}
		return con;
	}
}