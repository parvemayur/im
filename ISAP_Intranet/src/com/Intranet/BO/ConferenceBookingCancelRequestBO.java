package com.Intranet.BO;

import com.Intranet.DAO.ConferenceBookingCancelRequestDAO;
import com.Intranet.DAOImpl.ConferenceBookingCancelRequestDAOImpl;
import com.Intranet.JavaBean.ConferenceBookingCancelRequestBean;

public class ConferenceBookingCancelRequestBO {

	public int requestCancelBooking(ConferenceBookingCancelRequestBean bean) {
		// TODO Auto-generated method stub
		ConferenceBookingCancelRequestDAO dao=new ConferenceBookingCancelRequestDAOImpl();
		return dao.requestCancelBooking(bean);
	}

}
