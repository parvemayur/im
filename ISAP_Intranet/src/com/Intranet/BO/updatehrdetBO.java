package com.Intranet.BO;

import com.Intranet.DAO.updatehrdetDAO;
import com.Intranet.DAO.updatepolicydetDAO;
import com.Intranet.DAOImpl.updatehrdetDAOImpl;
import com.Intranet.JavaBean.updatehrdetBean;

public class updatehrdetBO {

	public boolean updatehr(updatehrdetBean bean) {
		
		updatehrdetDAO updao=new updatehrdetDAOImpl();
		Boolean flag=updao.proceed(bean);
		return flag;
	}

}
