package com.Intranet.JavaBean;

import javax.servlet.http.HttpServletRequest;

public class updatechairmanmsgBean {

	String cmsg,cyear,cstatus;

	public String getCmsg() {
		return cmsg;
	}

	public void setCmsg(String cmsg) {
		this.cmsg = cmsg;
	}

	public String getCyear() {
		return cyear;
	}

	public void setCyear(String cyear) {
		this.cyear = cyear;
	}

	public String getCstatus() {
		return cstatus;
	}

	public void setCstatus(String cstatus) {
		this.cstatus = cstatus;
	}
	
	public updatechairmanmsgBean(HttpServletRequest request){
		this.cmsg=request.getParameter("cmsg");

		this.cyear=request.getParameter("cmsgyear");
		this.cstatus=request.getParameter("cmsgstatus");
	}
}
