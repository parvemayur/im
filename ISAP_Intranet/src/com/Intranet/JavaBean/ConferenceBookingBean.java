package com.Intranet.JavaBean;

public class ConferenceBookingBean {
	int conf_booking_id, conf_master_id, emp_master_id, conf_booking_status;
	String conf_booking_timestart, conf_booking_timeend, conf_booking_type;

	public int getConf_booking_id() {
		return conf_booking_id;
	}

	public void setConf_booking_id(int conf_booking_id) {
		this.conf_booking_id = conf_booking_id;
	}

	public int getConf_master_id() {
		return conf_master_id;
	}

	public void setConf_master_id(int conf_master_id) {
		this.conf_master_id = conf_master_id;
	}

	public int getEmp_master_id() {
		return emp_master_id;
	}

	public void setEmp_master_id(int emp_master_id) {
		this.emp_master_id = emp_master_id;
	}

	public int getConf_booking_status() {
		return conf_booking_status;
	}

	public void setConf_booking_status(int conf_booking_status) {
		this.conf_booking_status = conf_booking_status;
	}

	public String getConf_booking_timestart() {
		return conf_booking_timestart;
	}

	public void setConf_booking_timestart(String conf_booking_timestart) {
		this.conf_booking_timestart = conf_booking_timestart;
	}

	public String getConf_booking_timeend() {
		return conf_booking_timeend;
	}

	public void setConf_booking_timeend(String conf_booking_timeend) {
		this.conf_booking_timeend = conf_booking_timeend;
	}

	public String getConf_booking_type() {
		return conf_booking_type;
	}

	public void setConf_booking_type(String conf_booking_type) {
		this.conf_booking_type = conf_booking_type;
	}

}
