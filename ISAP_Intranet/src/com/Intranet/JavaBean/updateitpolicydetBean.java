package com.Intranet.JavaBean;

import javax.servlet.http.HttpServletRequest;

public class updateitpolicydetBean {
	
	String itptitle,itpdetails,validpto,validpfrm,sortorder,status;

	public String getItptitle() {
		return itptitle;
	}

	public void setItptitle(String itptitle) {
		this.itptitle = itptitle;
	}

	public String getItpdetails() {
		return itpdetails;
	}

	public void setItpdetails(String itpdetails) {
		this.itpdetails = itpdetails;
	}

	public String getValidpto() {
		return validpto;
	}

	public void setValidpto(String validpto) {
		this.validpto = validpto;
	}

	public String getValidpfrm() {
		return validpfrm;
	}

	public void setValidpfrm(String validpfrm) {
		this.validpfrm = validpfrm;
	}

	public String getSortorder() {
		return sortorder;
	}

	public void setSortorder(String sortorder) {
		this.sortorder = sortorder;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	
	
	 public updateitpolicydetBean(HttpServletRequest request) {

	    this.itptitle = request.getParameter("ptitle");
	    this.itpdetails = request.getParameter("pdetails");
	    this.validpfrm=request.getParameter("validpfrm");

	    this.validpto=request.getParameter("validpto");

	    this.sortorder=request.getParameter("sortorder");

	    this.status=request.getParameter("txtstatus");
	}	

}
