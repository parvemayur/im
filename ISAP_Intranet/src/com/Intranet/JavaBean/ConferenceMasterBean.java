package com.Intranet.JavaBean;

public class ConferenceMasterBean {
	int conf_master_id, conf_master_status, conf_master_seatcount;
	String conf_master_name, conf_master_plant, conf_master_descr;

	public int getConf_master_id() {
		return conf_master_id;
	}

	public void setConf_master_id(int conf_master_id) {
		this.conf_master_id = conf_master_id;
	}

	public int getConf_master_status() {
		return conf_master_status;
	}

	public void setConf_master_status(int conf_master_status) {
		this.conf_master_status = conf_master_status;
	}

	public int getConf_master_seatcount() {
		return conf_master_seatcount;
	}

	public void setConf_master_seatcount(int conf_master_seatcount) {
		this.conf_master_seatcount = conf_master_seatcount;
	}

	public String getConf_master_name() {
		return conf_master_name;
	}

	public void setConf_master_name(String conf_master_name) {
		this.conf_master_name = conf_master_name;
	}

	public String getConf_master_plant() {
		return conf_master_plant;
	}

	public void setConf_master_plant(String conf_master_plant) {
		this.conf_master_plant = conf_master_plant;
	}

	public String getConf_master_descr() {
		return conf_master_descr;
	}

	public void setConf_master_descr(String conf_master_descr) {
		this.conf_master_descr = conf_master_descr;
	}

}
