package com.Intranet.JavaBean;

public class ConferenceBookingCancelRequestBean {
	int conf_booking_cancel_request_id, conf_booking_cancel_request_booking_id,
			conf_booking_cancel_request_to_employee_id,
			conf_booking_cancel_request_from_employee_id,
			conf_booking_cancel_request_status;
	String conf_booking_cancel_request_to_employee_email,
			conf_booking_cancel_request_to_employee_name;

	String conf_booking_cancel_request_descr;

	public String getConf_booking_cancel_request_to_employee_name() {
		return conf_booking_cancel_request_to_employee_name;
	}

	public void setConf_booking_cancel_request_to_employee_name(
			String conf_booking_cancel_request_to_employee_name) {
		this.conf_booking_cancel_request_to_employee_name = conf_booking_cancel_request_to_employee_name;
	}

	public String getConf_booking_cancel_request_to_employee_email() {
		return conf_booking_cancel_request_to_employee_email;
	}

	public void setConf_booking_cancel_request_to_employee_email(
			String conf_booking_cancel_request_to_employee_email) {
		this.conf_booking_cancel_request_to_employee_email = conf_booking_cancel_request_to_employee_email;
	}

	public int getConf_booking_cancel_request_id() {
		return conf_booking_cancel_request_id;
	}

	public void setConf_booking_cancel_request_id(
			int conf_booking_cancel_request_id) {
		this.conf_booking_cancel_request_id = conf_booking_cancel_request_id;
	}

	public int getConf_booking_cancel_request_booking_id() {
		return conf_booking_cancel_request_booking_id;
	}

	public void setConf_booking_cancel_request_booking_id(
			int conf_booking_cancel_request_booking_id) {
		this.conf_booking_cancel_request_booking_id = conf_booking_cancel_request_booking_id;
	}

	public int getConf_booking_cancel_request_to_employee_id() {
		return conf_booking_cancel_request_to_employee_id;
	}

	public void setConf_booking_cancel_request_to_employee_id(
			int conf_booking_cancel_request_to_employee_id) {
		this.conf_booking_cancel_request_to_employee_id = conf_booking_cancel_request_to_employee_id;
	}

	public int getConf_booking_cancel_request_from_employee_id() {
		return conf_booking_cancel_request_from_employee_id;
	}

	public void setConf_booking_cancel_request_from_employee_id(
			int conf_booking_cancel_request_from_employee_id) {
		this.conf_booking_cancel_request_from_employee_id = conf_booking_cancel_request_from_employee_id;
	}

	public int getConf_booking_cancel_request_status() {
		return conf_booking_cancel_request_status;
	}

	public void setConf_booking_cancel_request_status(
			int conf_booking_cancel_request_status) {
		this.conf_booking_cancel_request_status = conf_booking_cancel_request_status;
	}

	public String getConf_booking_cancel_request_descr() {
		return conf_booking_cancel_request_descr;
	}

	public void setConf_booking_cancel_request_descr(
			String conf_booking_cancel_request_descr) {
		this.conf_booking_cancel_request_descr = conf_booking_cancel_request_descr;
	}

}
