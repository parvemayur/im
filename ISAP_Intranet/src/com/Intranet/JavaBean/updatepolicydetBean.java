package com.Intranet.JavaBean;

import javax.servlet.http.HttpServletRequest;

public class updatepolicydetBean {
String ptitle,pdetails,validpto,validpfrm,sortorder,status;

public String getPtitle() {
	return ptitle;
}

public void setPtitle(String ptitle) {
	this.ptitle = ptitle;
}

public String getPdetails() {
	return pdetails;
}

public void setPdetails(String pdetails) {
	this.pdetails = pdetails;
}


public String getValidpto() {
	return validpto;
}

public void setValidpto(String validpto) {
	this.validpto = validpto;
}

public String getValidpfrm() {
	return validpfrm;
}


public String getSortorder() {
	return sortorder;
}

public void setSortorder(String sortorder) {
	this.sortorder = sortorder;
}

public String getStatus() {
	return status;
}

public void setStatus(String status) {
	this.status = status;
}

public void setValidpfrm(String validpfrm) {
	this.validpfrm = validpfrm;
}


public updatepolicydetBean(HttpServletRequest request) {

    this.ptitle = request.getParameter("ptitle");
    this.pdetails = request.getParameter("pdetails");
    this.validpfrm=request.getParameter("validpfrm");

    this.validpto=request.getParameter("validpto");

    this.sortorder=request.getParameter("sortorder");

    this.status=request.getParameter("txtstatus");
}	
}
