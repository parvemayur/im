package com.Intranet.DAO;

import com.Intranet.JavaBean.ConferenceBookingBean;

public interface ConferenceBookingDAO {

	int addConferenceBooking(ConferenceBookingBean bean);

}
