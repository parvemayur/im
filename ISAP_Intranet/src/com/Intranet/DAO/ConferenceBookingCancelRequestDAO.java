package com.Intranet.DAO;

import com.Intranet.JavaBean.ConferenceBookingCancelRequestBean;

public interface ConferenceBookingCancelRequestDAO {

	int requestCancelBooking(ConferenceBookingCancelRequestBean bean);

}
