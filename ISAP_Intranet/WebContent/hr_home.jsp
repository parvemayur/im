<%@page import="com.Intranet.Util.doConnection"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<jsp:include page="headerscripts.jsp"></jsp:include>
</head>
<body class="page-header-fixed page-quick-sidebar-over-content ">
	<!-- BEGIN HEADER -->

	<jsp:include page="header.jsp"></jsp:include>
	<!-- END HEADER -->
	<div class="clearfix"></div>
	<!-- BEGIN CONTAINER -->
	<div class="page-container">
		<!-- BEGIN SIDEBAR -->

		<jsp:include page="sidebar.jsp"></jsp:include>
		<!-- END SIDEBAR -->
		<!-- BEGIN CONTENT -->
		<div class="page-content-wrapper">
			<div class="page-content">
				<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
				<div class="modal fade" id="portlet-config" tabindex="-1"
					role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal"
									aria-hidden="true"></button>
								<h4 class="modal-title">Modal title</h4>
							</div>
							<div class="modal-body">Widget settings form goes here</div>
							<div class="modal-footer">
								<button type="button" class="btn blue">Save changes</button>
								<button type="button" class="btn default" data-dismiss="modal">Close</button>
							</div>
						</div>
						<!-- /.modal-content -->
					</div>
					<!-- /.modal-dialog -->
				</div>
				<!-- /.modal -->
				<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
				<!-- BEGIN STYLE CUSTOMIZER -->
				<div class="theme-panel hidden-xs hidden-sm">
					<div class="toggler"></div>
					<div class="toggler-close"></div>

				</div>
				<!-- END STYLE CUSTOMIZER -->
				<!-- BEGIN PAGE HEADER-->
				<h3 class="page-title">
					Dashboard
				</h3>
				<div class="page-bar">
					<ul class="page-breadcrumb">
						<li><i class="fa fa-home"></i> <a href="index.html">HR</a>
							<i class="fa fa-angle-right"></i></li>
						<li><a href="#">Policy</a>
						</li>
					</ul>
					
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
			
					<div class="row ">
				<div class="col-md-6 col-sm-6">
				<!-- BEGIN PORTLET-->
				
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-bell-o"></i>HR Policies
							</div>
							<div class="actions">
								<div class="btn-group">
									<a class="btn btn-sm btn-default dropdown-toggle" href="#" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
									Filter By <i class="fa fa-angle-down"></i>
									</a>
									<div class="dropdown-menu hold-on-click dropdown-checkboxes pull-right">
										<label><input type="checkbox"/>Last Updated</label>
										</div>
								</div>
							</div>
						</div>
						<div class="portlet-body">
							<div class="scroller" style="height: 300px;" data-always-visible="1" data-rail-visible="0">
								<ul class="feeds">
										
								<%
								PreparedStatement ps1=null;
			                    ResultSet rs1=null;
			                    Connection con=null;
			                    con=doConnection.getConnection();
			                    ps1=con.prepareStatement("select  hr_policy_id,hr_policy_name, hr_policy_details, hr_policy_sort,hr_policy_validfrom,hr_policy_validto from hr_policy where hr_policy_status <> 'Disable' order by hr_policy_sort; ");
			                    
			                   

			                    System.out.println("ps1=="+ps1);
			                    rs1=ps1.executeQuery();
			                    
			                     while(rs1.next()){   
			                         
			                   
								
								%>
									
									<li>
										<div class="col1">
											<div class="cont">
												<div class="cont-col1">
													<div class="label label-sm label-info">
														<i class="fa fa-check"></i>
													</div>
												</div>
													<%
												   SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
                                                SimpleDateFormat  sdf2 = new SimpleDateFormat("dd-MM-yyyy");
                                                Date date = null;
                                                date = (Date) sdf1.parse(rs1.getString(5));

                                                String DisplayDate = sdf2.format(date);
                                               
												
												
												%>
								
												<div class="cont-col2">
													<div class="desc">
														<a href="hrpolicy_userview_description.jsp?id=<%=rs1.getString(1)%>"> <%=rs1.getString(2) %> </a>
														
														
													</div>
													
												</div>
												
										
											</div>
											 
										</div>
										
										<div class="col2">
											<div class="date">
												<%=DisplayDate %>  
											</div>
										</div>
									</li>
									<%} %>
									
								</ul>
							</div>
							<div class="scroller-footer">
								<div class="btn-arrow-link pull-right">
									<a href="#">See All Records</a>
									<i class="icon-arrow-right"></i>
								</div>
							</div>
						</div>
					</div>
					
					<!-- END PORTLET-->
				</div>
			
			
				<div class="col-md-6 col-sm-6">
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-bell-o"></i>HR Update
							</div>
							<div class="actions">
								<div class="btn-group">
									<a class="btn btn-sm btn-default dropdown-toggle" href="#" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
									Filter By <i class="fa fa-angle-down"></i>
									</a>
									<div class="dropdown-menu hold-on-click dropdown-checkboxes pull-right">
										<label><input type="checkbox"/>Last Updated</label>
										</div>
								</div>
							</div>
						</div>
						<div class="portlet-body">
							<div class="scroller" style="height: 300px;" data-always-visible="1" data-rail-visible="0">
								<ul class="feeds">
										
								<%
								PreparedStatement ps2=null;
			                    ResultSet rs2=null;
			                   // Connection con=null;
			                    con=doConnection.getConnection();
			                    ps2=con.prepareStatement("select  hr_update_id,hr_update_name,hr_update_details,hr_update_sort,hr_update_validfrom,hr_update_validto,hr_update_status from hr_updates where hr_update_status <> 'Disable' order by hr_update_sort; ");
			                     
			                   

			                    System.out.println("ps1=="+ps1);
			                    rs2=ps2.executeQuery();
			                    
			                     while(rs2.next()){   
			                         
			                   
								
								%>
									
									<li>
										<div class="col1">
											<div class="cont">
												<div class="cont-col1">
													<div class="label label-sm label-info">
														<i class="fa fa-check"></i>
													</div>
												</div>
													<%
												   SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
                                                SimpleDateFormat  sdf2 = new SimpleDateFormat("dd-MM-yyyy");
                                                Date date = null;
                                                date = (Date) sdf1.parse(rs2.getString(5));

                                                String DisplayDate = sdf2.format(date);
                                               
												
												
												%>
								
												<div class="cont-col2">
													<div class="desc">
														<a href="hrupdate_userview_description.jsp?id=<%=rs2.getString(1)%>" title="Click on policy name to view in details"> <%=rs2.getString(2) %> </a>
														
														
													</div>
													
												</div>
												
										
											</div>
											 
										</div>
										
										<div class="col2">
											<div class="date">
												<%=DisplayDate %>  
											</div>
										</div>
									</li>
									<%} %>
									
								</ul>
							</div>
							<div class="scroller-footer">
								<div class="btn-arrow-link pull-right">
									<a href="#">See All Records</a>
									<i class="icon-arrow-right"></i>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
					
					
											
			
			<!-- END PORTLET-->

				
				<!-- END PAGE CONTENT-->
			</div>
		</div>
		<!-- END CONTENT -->
		<!-- BEGIN QUICK SIDEBAR -->

		<jsp:include page="quicksidebar.jsp"></jsp:include>
		<!-- END QUICK SIDEBAR -->
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->

	<jsp:include page="footer.jsp"></jsp:include>
	<!-- END FOOTER -->

	<jsp:include page="footerscripts.jsp"></jsp:include>
</body>
<!-- END BODY -->
</html>