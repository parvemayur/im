<%@page import="com.Intranet.Util.Emailing"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="com.Intranet.Util.doConnection"%>
<%@page import="java.sql.Connection"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%
	if (session.getAttribute("userid") != null) {
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<jsp:include page="headerscripts.jsp"></jsp:include>
</head>
<body class="page-header-fixed page-quick-sidebar-over-content ">
	<!-- BEGIN HEADER -->
	<%
		if (request.getParameter("flag") != null
					&& request.getParameter("flag").equals("1")) {
				Connection con = doConnection.getConnection();
				con.setAutoCommit(false);
				PreparedStatement ps = con
						.prepareStatement("update conference_booking set conf_booking_status=? where conf_booking_id=?");
				ps.setInt(1, 0);
				ps.setInt(2,
						Integer.parseInt(request.getParameter("bookingid")));
				int result = ps.executeUpdate();

				PreparedStatement ps1 = con
						.prepareStatement("update conf_booking_cancel_request set conf_booking_cancel_request_status=? where conf_booking_cancel_request_id=?");
				ps1.setInt(1, 1);
				ps1.setInt(2,
						Integer.parseInt(request.getParameter("cancelid")));
				int result1 = ps1.executeUpdate();
				if (result == 1 && result1 == 1) {
					con.commit();
					session.setAttribute("alert",
							"Meeting canceled successfully...");
					try
					{
						String message="Dear "+request.getParameter("empname")+"<br/><br/>";
						message+=session.getAttribute("username")+" has approved your reschedule request.<br/>";
						message+="For more details please login to portal.";
						
						Emailing.sendmail(request.getParameter("empname"), request.getParameter("empemail"), "Conference reschedule approval notification", message);
					}
					catch(Exception e)
					{
						
					}
				} else {
					con.rollback();
					session.setAttribute("alert", "Problem occured...");
				}
			}
	%>

	<%
		if (request.getParameter("flag") != null
					&& request.getParameter("flag").equals("2")) {
				Connection con = doConnection.getConnection();
				con.setAutoCommit(false);

				PreparedStatement ps1 = con
						.prepareStatement("update conf_booking_cancel_request set conf_booking_cancel_request_status=? where conf_booking_cancel_request_id=?");
				ps1.setInt(1, 2);
				ps1.setInt(2,
						Integer.parseInt(request.getParameter("cancelid")));
				int result1 = ps1.executeUpdate();
				if (result1 == 1) {
					con.commit();
					session.setAttribute("alert",
							"Intimation sent to user...");
					try
					{
						String message="Dear "+request.getParameter("empname")+"<br/><br/>";
						message+=session.getAttribute("username")+" has denied your reschedule request.<br/>";
						message+="For more details please login to portal.";
						
						Emailing.sendmail(request.getParameter("empname"), request.getParameter("empemail"), "Conference reschedule deniel notification", message);
					}
					catch(Exception e)
					{
						
					}
				} else {
					con.rollback();
					session.setAttribute("alert", "Problem occured...");
				}
			}
	%>
	<jsp:include page="header.jsp"></jsp:include>
	<!-- END HEADER -->
	<div class="clearfix"></div>
	<!-- BEGIN CONTAINER -->
	<div class="page-container">
		<!-- BEGIN SIDEBAR -->

		<jsp:include page="sidebar.jsp"></jsp:include>
		<!-- END SIDEBAR -->
		<!-- BEGIN CONTENT -->
		<div class="page-content-wrapper">
			<div class="page-content">

				<!-- BEGIN PAGE HEADER-->
				<h3 class="page-title">Conference Notification</h3>
				<div class="page-bar">
					<ul class="page-breadcrumb">
						<li><i class="fa fa-home"></i> <a href="index.html">Conference</a>
							<i class="fa fa-angle-right"></i></li>
						<li><a href="#">Conference Notification</a></li>
					</ul>
				</div>



				<%
					if (session.getAttribute("alert") != null) {
				%>
				<div class="row">
					<div class="col-md-12">
						<div class="alert alert-danger">
							<button class="close" data-close="alert"></button>
							<%=session.getAttribute("alert")%>
						</div>
					</div>
				</div>
				<%
					session.removeAttribute("alert");
						}
				%>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->

				<!-- END PAGE CONTENT-->

				<div class="row">
					<div class="col-md-12">
						<div class="portlet box blue-madison">
							<div class="portlet-title">
								<div class="caption">
									<i class="fa fa-globe"></i>Conference Notification List
								</div>

							</div>
							<div class="portlet-body">
								<table class="table table-striped table-bordered table-hover"
									id="sample_6">
									<thead>
										<tr>
											<th></th>
											<th>Conference room</th>
											<th>Conference type</th>
											<th>Conference start</th>
											<th>Conference end</th>
											<th>Request from</th>
											<th>Description</th>
										</tr>
									</thead>
									<tbody>
										<%
											try {
													Connection con = doConnection.getConnection();
													PreparedStatement ps = con
															.prepareStatement("select em.employee_master_email,em.employee_master_name,cb.conf_booking_type,cbcr.conf_booking_cancel_request_id,cbcr.conf_booking_cancel_request_booking_id,cbcr.conf_booking_cancel_request_from_employee_id,cm.conf_master_name,CONVERT(VARCHAR(24),cb.conf_booking_timestart,113) as conf_booking_timestart,CONVERT(VARCHAR(24),cb.conf_booking_timeend,113) as conf_booking_timeend,cbcr.conf_booking_cancel_request_from_employee_id,cbcr.conf_booking_cancel_request_descr from conf_booking_cancel_request cbcr,conference_booking cb,conference_master cm,employee_master em where cbcr.conf_booking_cancel_request_booking_id=cb.conf_booking_id and cb.conf_master_id=cm.conf_master_id and cbcr.conf_booking_cancel_request_status=0 and cbcr.conf_booking_cancel_request_from_employee_id=em.employee_master_id and CAST(cb.conf_booking_timestart AS DATE)>=CAST(getDate() AS DATE) and cbcr.conf_booking_cancel_request_status=0 and cbcr.conf_booking_cancel_request_to_employee_id=?");
													ps.setInt(
															1,
															Integer.parseInt(""
																	+ session.getAttribute("userid")));
													ResultSet rs = ps.executeQuery();
													while (rs.next()) {
										%>
										<tr>
											<td><div class="btn-group">
													<button class="btn btn-default btn-xs dropdown-toggle"
														type="button" data-toggle="dropdown">
														Action <i class="fa fa-angle-down"></i>
													</button>
													<ul class="dropdown-menu" role="menu">
														<li><a
															href="conference_notification.jsp?flag=1&cancelid=<%=rs
								.getString("conf_booking_cancel_request_id")%>&bookingid=<%=rs
								.getString("conf_booking_cancel_request_booking_id")%>&empid=<%=rs
								.getString("conf_booking_cancel_request_from_employee_id")%>&empname=<%=rs
								.getString("employee_master_name")%>&empemail=<%=rs
								.getString("employee_master_email")%>">
																Approve </a></li>
														<li><a
															href="conference_notification.jsp?flag=2&cancelid=<%=rs
								.getString("conf_booking_cancel_request_id")%>&bookingid=<%=rs
								.getString("conf_booking_cancel_request_booking_id")%>&empid=<%=rs
								.getString("conf_booking_cancel_request_from_employee_id")%>&empname=<%=rs
								.getString("employee_master_name")%>&empemail=<%=rs
								.getString("employee_master_email")%>">
																Deny </a></li>
													</ul>
												</div></td>
											<td><%=rs.getString("conf_master_name")%></td>
											<td><%=rs.getString("conf_booking_type")%></td>
											<td><%=rs.getString("conf_booking_timestart")%></td>
											<td><%=rs.getString("conf_booking_timeend")%></td>
											<td><%=rs.getString("employee_master_name")%></td>
											<td><%=rs
								.getString("conf_booking_cancel_request_descr")%></td>
										</tr>
										<%
											}
												} catch (Exception e) {
													e.printStackTrace();
												}
										%>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- END CONTENT -->
		<!-- BEGIN QUICK SIDEBAR -->

		<jsp:include page="quicksidebar.jsp"></jsp:include>
		<!-- END QUICK SIDEBAR -->
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->

	<jsp:include page="footer.jsp"></jsp:include>
	<!-- END FOOTER -->

	<jsp:include page="footerscripts.jsp"></jsp:include>
</body>
<!-- END BODY -->
</html>
<%
	} else {
		response.sendRedirect("index.jsp");
	}
%>