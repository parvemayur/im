<%@page import="com.google.gson.Gson"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.Intranet.JavaBean.ConferenceEventsBean"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="com.Intranet.Util.doConnection"%>
<%@page import="java.sql.Connection"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%
String json="";
	try {
		Connection con = doConnection.getConnection();
		PreparedStatement ps = con
				.prepareStatement("select cb.conf_booking_id,em.employee_master_id,em.employee_master_name,em.employee_master_email,cb.conf_booking_type,cm.conf_master_name,CONVERT(VARCHAR(24),cb.conf_booking_timestart,126) as conf_booking_timestart,CONVERT(VARCHAR(24),cb.conf_booking_timeend,126) as conf_booking_timeend from conference_booking cb,conference_master cm,employee_master em where cb.conf_master_id=cm.conf_master_id and cb.emp_master_id=em.employee_master_id and CAST(cb.conf_booking_timestart AS DATE)>=CAST(getDate() AS DATE) and cb.conf_booking_status=1");
		ResultSet rs = ps.executeQuery();
		ArrayList<ConferenceEventsBean> eventsList=new ArrayList<ConferenceEventsBean>();
		while (rs.next()) {
			ConferenceEventsBean bean=new ConferenceEventsBean();
			bean.setTitle(rs.getString("employee_master_name"));
			bean.setDescription(rs.getString("conf_master_name")+"<br/>"+rs.getString("conf_booking_type"));
			bean.setStart(rs.getString("conf_booking_timestart"));
			bean.setEnd(rs.getString("conf_booking_timeend"));
			bean.setBackgroundColor("#35a59a");
			bean.setBookingid(rs.getString("conf_booking_id"));
			bean.setEmpid(rs.getString("employee_master_id"));
			bean.setEmpemail(rs.getString("employee_master_email"));
			bean.setEmpname(rs.getString("employee_master_name"));
			
			eventsList.add(bean);
		}
		json = new Gson().toJson(eventsList);
	} catch (Exception e) {
		e.printStackTrace();
	}
%>
<script>
	var Calendar = function() {

		return {
			//main function to initiate the module
			init : function() {
				Calendar.initCalendar();
			},

			initCalendar : function() {

				if (!jQuery().fullCalendar) {
					return;
				}

				var date = new Date();
				var d = date.getDate();
				var m = date.getMonth();
				var y = date.getFullYear();

				var h = {};

				if (Metronic.isRTL()) {
					if ($('#calendar').parents(".portlet").width() <= 720) {
						$('#calendar').addClass("mobile");
						h = {
							right : 'title, prev, next',
							center : '',
							left : 'agendaDay, agendaWeek, month, today'
						};
					} else {
						$('#calendar').removeClass("mobile");
						h = {
							right : 'title',
							center : '',
							left : 'agendaDay, agendaWeek, month, today, prev,next'
						};
					}
				} else {
					if ($('#calendar').parents(".portlet").width() <= 720) {
						$('#calendar').addClass("mobile");
						h = {
							left : 'title, prev, next',
							center : '',
							right : 'today,month,agendaWeek,agendaDay'
						};
					} else {
						$('#calendar').removeClass("mobile");
						h = {
							left : 'title',
							center : '',
							right : 'prev,next,today,month,agendaWeek,agendaDay'
						};
					}
				}

				var initDrag = function(el) {
					// create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
					// it doesn't need to have a start or end
					var eventObject = {
						title : $.trim(el.text())
					// use the element's text as the event title
					};
					// store the Event Object in the DOM element so we can get to it later
					el.data('eventObject', eventObject);
					// make the event draggable using jQuery UI
					el.draggable({
						zIndex : 999,
						revert : true, // will cause the event to go back to its
						revertDuration : 0
					//  original position after the drag
					});
				};

				var addEvent = function(title) {
					title = title.length === 0 ? "Untitled Event" : title;
					var html = $('<div class="external-event label label-default">'
							+ title + '</div>');
					jQuery('#event_box').append(html);
					initDrag(html);
				};

				$('#external-events div.external-event').each(function() {
					initDrag($(this));
				});

				$('#event_add').unbind('click').click(function() {
					var title = $('#event_title').val();
					addEvent(title);
				});

				//predefined events
				$('#event_box').html("");
				addEvent("My Event 1");
				addEvent("My Event 2");
				addEvent("My Event 3");
				addEvent("My Event 4");
				addEvent("My Event 5");
				addEvent("My Event 6");

				$('#calendar').fullCalendar('destroy'); // destroy the calendar
				$('#calendar')
						.fullCalendar(
								{ //re-initialize the calendar
									header : h,
									defaultView : 'month', // change default view with available options from http://arshaw.com/fullcalendar/docs/views/Available_Views/ 
									slotMinutes : 15,
									editable : true,
									droppable : true, // this allows things to be dropped onto the calendar !!!
									drop : function(date, allDay) { // this function is called when something is dropped

										// retrieve the dropped element's stored Event Object
										var originalEventObject = $(this).data(
												'eventObject');
										// we need to copy it, so that multiple events don't have a reference to the same object
										var copiedEventObject = $.extend({},
												originalEventObject);

										// assign it the date that was reported
										copiedEventObject.start = date;
										copiedEventObject.allDay = allDay;
										copiedEventObject.className = $(this)
												.attr("data-class");

										// render the event on the calendar
										// the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
										$('#calendar').fullCalendar(
												'renderEvent',
												copiedEventObject, true);

										// is the "remove after drop" checkbox checked?
										if ($('#drop-remove').is(':checked')) {
											// if so, remove the element from the "Draggable Events" list
											$(this).remove();
										}
									},
									dayClick : function(start, end, allDay,
											jsEvent, view) {
										$('#conf_date').val(
												start.format('MM/DD/YYYY'));
										$('#scheduleconfModal').modal();
									},
									eventClick : function(event, jsEvent, view) {
										$('#conf_booking_cancel_request_to_employee_id').val(event.empid);
										$('#conf_booking_cancel_request_booking_id').val(event.bookingid);
										$('#conf_booking_cancel_request_to_employee_email').val(event.empemail);
										$('#conf_booking_cancel_request_to_employee_name').val(event.empname);
										$('#modalTitle').html(
												"<b>" + event.title + "</b>");
										$('#modalBody')
												.html(
														'<b>Start : '
																+ moment(
																		event.start)
																		.format(
																				'MMM Do h:mm A')
																+ '<br/>End : '
																+ moment(
																		event.end)
																		.format(
																				'MMM Do h:mm A')
																+ '</b><br/><br/>'
																+ event.description);
										$('#eventUrl').attr('href', event.url);
										$('#fullCalModal').modal();
										if (event.url) {
											//window.open(event.url);
											return false;
										}
									},
									events : <%=json %>
								});

			}

		};

	}();
</script>