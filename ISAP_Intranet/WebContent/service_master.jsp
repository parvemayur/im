<%@page import="com.Intranet.Util.doConnection"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.text.Format"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<jsp:include page="headerscripts.jsp"></jsp:include>
</head>
<body class="page-header-fixed page-quick-sidebar-over-content ">
	<!-- BEGIN HEADER -->

	<jsp:include page="header.jsp"></jsp:include>
	<!-- END HEADER -->
	<div class="clearfix"></div>
	<!-- BEGIN CONTAINER -->
	<div class="page-container">
		<!-- BEGIN SIDEBAR -->

		<jsp:include page="sidebar.jsp"></jsp:include>
		<!-- END SIDEBAR -->
		<!-- BEGIN CONTENT -->
		<div class="page-content-wrapper">
			<div class="page-content">
				<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
				<div class="modal fade" id="portlet-config" tabindex="-1"
					role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal"
									aria-hidden="true"></button>
								<h4 class="modal-title">Modal title</h4>
							</div>
							<div class="modal-body">Widget settings form goes here</div>
							<div class="modal-footer">
								<button type="button" class="btn blue">Save changes</button>
								<button type="button" class="btn default" data-dismiss="modal">Close</button>
							</div>
						</div>
						<!-- /.modal-content -->
					</div>
					<!-- /.modal-dialog -->
				</div>
				<!-- /.modal -->
				<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
				<!-- BEGIN STYLE CUSTOMIZER -->
				<div class="theme-panel hidden-xs hidden-sm">
					<div class="toggler"></div>
					<div class="toggler-close"></div>

				</div>
				<!-- END STYLE CUSTOMIZER -->
				<!-- BEGIN PAGE HEADER-->
				<h3 class="page-title">
					HR POLICIES
				</h3>
				<div class="page-bar">
					<ul class="page-breadcrumb">
						<li><i class="fa fa-home"></i> <a href="index.html">HR</a>
							<i class="fa fa-angle-right"></i></li>
						<li><a href="#">Chairmans Message</a>
						</li>
					</ul>
					<div class="page-toolbar">
						<div class="btn-group pull-right">
							<button type="button"
								class="btn btn-fit-height grey-salt dropdown-toggle"
								data-toggle="dropdown" data-hover="dropdown" data-delay="1000"
								data-close-others="true">
								Actions <i class="fa fa-angle-down"></i>
							</button>
							<ul class="dropdown-menu pull-right" role="menu">
								<li><a href="#">Action</a></li>
								<li><a href="#">Another action</a></li>
								<li><a href="#">Something else here</a></li>
								<li class="divider"></li>
								<li><a href="#">Separated link</a></li>
							</ul>
						</div>
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
			<!-- BEGIN PORTLET-->
					<div class="portlet box purple">
						
						<div class="portlet-body form">
							<!-- BEGIN VALIDATION STATES-->
					<div class="portlet box purple">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-gift"></i>Chairmans Message 
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="javascript:;" class="reload">
								</a>
								<a href="javascript:;" class="remove">
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<!-- BEGIN FORM-->
							<form action="./updatechairmanmsg" id="form_sample_2" name="chairmanmsg" class="form-horizontal">
								<div class="form-body">
									<div class="alert alert-danger display-hide">
										<button class="close" data-close="alert"></button>
										You have some form errors. Please check below.
									</div>
									<div class="alert alert-success display-hide">
										<button class="close" data-close="alert"></button>
										Your form validation is successful!
									</div>
									<div class="form-group">
										<label class="control-label col-md-3">Message <span class="required">
										* </span>
										</label>
										<div class="col-md-4">
											<input type="text" name="cmsg" id="cmsg" data-required="1" class="form-control"/>
										</div>
									</div>
									
									<%
			
									DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                                    DateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy");
                                    Date date1 = new Date();
                                    Date date2 = new Date();
                                   
                                    String date11 = dateFormat.format(date2);
                                   

                                    Format formatter1 = new SimpleDateFormat("yyyy");
                                    String s1 = formatter1.format(date1);
                                   
									int year=Integer.parseInt(s1);
									
									
									%> 
									
								<div class="form-group">
										<label class="control-label col-md-3">Year <span class="required">
										* </span>
										</label>
										<div class="col-md-4">
												<select class="form-control" name="cmsgyear"  id="cmsgyear" multiple>
												<% for(int i=year;i<=year+10;i++){
												%>
												<option value="<%=i%>"><%=i%></option>
												<%} %>
												</select>
									
										
								</div>
								</div>
								
								
									<div class="form-group">
										<label class="control-label col-md-3">Status <span class="required">
										* </span>
										</label>
										<div class="col-md-4">
											<select class="form-control" name="cmsgstatus"  id="cmsgstatus" multiple>
												<option value="Enable">Enable</option>
												<option value="Disable">Disable</option>
												</select>
											
										</div>
									</div>

								<div class="form-actions">
									<div class="row">
										<div class="col-md-offset-3 col-md-9">
											<button type="submit" class="btn green">Submit</button>
											<button type="button" class="btn default">Cancel</button>
										</div>
									</div>
								</div>
							</form>
							<!-- END FORM-->
						</div>
					</div>
					<!-- END VALIDATION STATES-->
						</div>
						
						
						
						
							
						<!-- BEGIN SAMPLE TABLE PORTLET-->
					<div class="portlet box purple">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-comments"></i>Chairmans Message
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="javascript:;" class="reload">
								</a>
								<a href="javascript:;" class="remove">
								</a>
							</div>
						</div>
						<div class="portlet-body">
							<div class="table-scrollable">
								<table class="table table-striped table-hover">
								<thead>
								<tr>
									<th>
										 ID
									</th>
									<th>
										  Message
									</th>
									<th>
										Year
									</th>
									
								</tr>
								</thead>
								<tbody>
								
								<%
								PreparedStatement ps1=null;
			                    ResultSet rs1=null;
			                    Connection con=null;
			                    con=doConnection.getConnection();
			                    ps1=con.prepareStatement("select  chairman_msg_id,chairman_msg_msgname,chairman_msg_year,chairman_msg_status from chairman_msg where chairman_msg_status <> 'Disable' order by chairman_msg_year; ");
			                    
			                   

			                    System.out.println("ps1=="+ps1);
			                    rs1=ps1.executeQuery();
			                    
			                     while(rs1.next()){   
			                         
			                   
								
								%>
								
								<tr>
									<td>
										<%=rs1.getString(1) %> 
									</td>
									<td>
										 <%=rs1.getString(2) %> 
									</td>
									<%
												   SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
                                                SimpleDateFormat  sdf2 = new SimpleDateFormat("dd-MM-yyyy");
                                                Date date = null;
                                                date = (Date) sdf1.parse(rs1.getString(3));

                                                String DisplayDate = sdf2.format(date);
                                               
												
												
												%>
									<td>
										 <%=DisplayDate %> 
									</td>
									
										
								</tr>
								 <%
                        
                }%>
								</tbody>
								</table>
							</div>
						</div>
					</div>
					<!-- END SAMPLE TABLE PORTLET-->
						
					</div>
					<!-- END PORTLET-->

				
				<!-- END PAGE CONTENT-->
			</div>
		</div>
		<!-- END CONTENT -->
		<!-- BEGIN QUICK SIDEBAR -->

		<jsp:include page="quicksidebar.jsp"></jsp:include>
		<!-- END QUICK SIDEBAR -->
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->

	<jsp:include page="footer.jsp"></jsp:include>
	<!-- END FOOTER -->

	<jsp:include page="footerscripts.jsp"></jsp:include>
</body>
<!-- END BODY -->
</html>