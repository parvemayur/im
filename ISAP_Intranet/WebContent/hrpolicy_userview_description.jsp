<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.Intranet.Util.doConnection"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<jsp:include page="headerscripts.jsp"></jsp:include>
</head>
<body class="page-header-fixed page-quick-sidebar-over-content ">
	<!-- BEGIN HEADER -->

	<jsp:include page="header.jsp"></jsp:include>
	<!-- END HEADER -->
	<div class="clearfix"></div>
	<!-- BEGIN CONTAINER -->
	<div class="page-container">
		<!-- BEGIN SIDEBAR -->

		<jsp:include page="sidebar.jsp"></jsp:include>
		<!-- END SIDEBAR -->
		<!-- BEGIN CONTENT -->
		<div class="page-content-wrapper">
			<div class="page-content">
				<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
				<div class="modal fade" id="portlet-config" tabindex="-1"
					role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal"
									aria-hidden="true"></button>
								<h4 class="modal-title">Modal title</h4>
							</div>
							<div class="modal-body">Widget settings form goes here</div>
							<div class="modal-footer">
								<button type="button" class="btn blue">Save changes</button>
								<button type="button" class="btn default" data-dismiss="modal">Close</button>
							</div>
						</div>
						<!-- /.modal-content -->
					</div>
					<!-- /.modal-dialog -->
				</div>
				<!-- /.modal -->
				<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
				<!-- BEGIN STYLE CUSTOMIZER -->
				<div class="theme-panel hidden-xs hidden-sm">
					<div class="toggler"></div>
					<div class="toggler-close"></div>

				</div>
				<!-- END STYLE CUSTOMIZER -->
				<!-- BEGIN PAGE HEADER-->
				<h3 class="page-title">
					HR POLICIES
				</h3>
				<div class="page-bar">
					<ul class="page-breadcrumb">
						<li><i class="fa fa-home"></i> <a href="index.html">HR</a>
							<i class="fa fa-angle-right"></i></li>
						<li><a href="#">POLICY DETAILS</a>
						</li>
					</ul>
					
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
			<!-- BEGIN PORTLET-->
					

					<!-- BEGIN BLOCKQUOTES PORTLET-->
					
					
					<%
					String pid=request.getParameter("id");
								PreparedStatement ps1=null;
			                    ResultSet rs1=null;
			                    Connection con=null;
			                    con=doConnection.getConnection();
			                    ps1=con.prepareStatement("select  hr_policy_name, hr_policy_details, hr_policy_sort,hr_policy_validfrom,hr_policy_validto from hr_policy where hr_policy_id=? ");
			                    ps1.setString(1, pid);
			                   

			                    System.out.println("ps1=="+ps1);
			                    rs1=ps1.executeQuery();
			                    
			                     while(rs1.next()){   
			                         
			                   
								
								%>
									<%
												   SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
                                                SimpleDateFormat  sdf2 = new SimpleDateFormat("dd-MM-yyyy");
                                                Date date = null,date1=null;
                                                date = (Date) sdf1.parse(rs1.getString(4));
                                                date1 = (Date) sdf1.parse(rs1.getString(5));
                                                String DisplayDate = sdf2.format(date);
                                                String DisplayDate1 = sdf2.format(date1);
												
												
												%>
							<div class="portlet purple box">
								
						
								<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-cogs"></i><%=rs1.getString(1) %>
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="javascript:;" class="reload">
								</a>
								<a href="javascript:;" class="remove">
								</a>
							</div>
						</div>
								
							
							
							
							<div class="portlet-body">
								<h3></h3>
								<div class="alert alert-success">
								<strong>Valid From </strong> <%=DisplayDate %> &nbsp;&nbsp;<strong>Valid To </strong> <%=DisplayDate1 %>
							</div>
								<blockquote>
									<p>
										 <%=rs1.getString(2) %>
									</p>
								</blockquote>
							
							</div>
						</div>
						
						<%}%>
						</div>
						<!-- END BLOCKQUOTES PORTLET-->
					
											
						
											<!-- END PORTLET-->


				
				<!-- END PAGE CONTENT-->
			</div>
		</div>
		<!-- END CONTENT -->
		<!-- BEGIN QUICK SIDEBAR -->

		<jsp:include page="quicksidebar.jsp"></jsp:include>
		<!-- END QUICK SIDEBAR -->
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->

	<jsp:include page="footer.jsp"></jsp:include>
	<!-- END FOOTER -->

	<jsp:include page="footerscripts.jsp"></jsp:include>
</body>
<!-- END BODY -->
</html>