<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="com.Intranet.Util.doConnection"%>
<%@page import="java.sql.Connection"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%
if(session.getAttribute("userid")!=null)
{
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<jsp:include page="headerscripts.jsp"></jsp:include>
</head>
<body class="page-header-fixed page-quick-sidebar-over-content ">
	<!-- BEGIN HEADER -->

	<jsp:include page="header.jsp"></jsp:include>
	<!-- END HEADER -->
	<div class="clearfix"></div>
	<!-- BEGIN CONTAINER -->
	<div class="page-container">
		<!-- BEGIN SIDEBAR -->

		<jsp:include page="sidebar.jsp"></jsp:include>
		<!-- END SIDEBAR -->
		<!-- BEGIN CONTENT -->
		<div class="page-content-wrapper">
			<div class="page-content">

				<!-- BEGIN PAGE HEADER-->
				<h3 class="page-title">Conference Room Booking </h3>
				<div class="page-bar">
					<ul class="page-breadcrumb">
						<li><i class="fa fa-home"></i> <a href="index.html">Conference</a>
							<i class="fa fa-angle-right"></i></li>
						<li><a href="#">Conference Room Booking</a></li>
					</ul>
				</div>
				<%
					if (session.getAttribute("alert") != null) {
				%>
				<div class="row">
					<div class="col-md-12">
						<div class="alert alert-danger">
							<button class="close" data-close="alert"></button>
							<%=session.getAttribute("alert")%>
						</div>
					</div>
				</div>
				<%
					session.removeAttribute("alert");
					}
				%>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->


				<div class="row">

					<div class="col-md-12">
						<!-- BEGIN VALIDATION STATES-->
						<div class="portlet box blue-madison calendar">
							<div class="portlet-title">
								<div class="caption">
									<i class="fa fa-calendar"></i>Conference room booking details
								</div>
							</div>
							<div class="portlet-body light-grey">
								<div id="calendar"></div>
							</div>
						</div>
						<!-- END VALIDATION STATES-->
					</div>
				</div>
				<!-- END PAGE CONTENT-->
			</div>
		</div>
		<!-- END CONTENT -->
		<!-- BEGIN QUICK SIDEBAR -->

		<jsp:include page="quicksidebar.jsp"></jsp:include>
		<!-- END QUICK SIDEBAR -->
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->

	<jsp:include page="footer.jsp"></jsp:include>
	<!-- END FOOTER -->
<jsp:include page="loadConferences.jsp"></jsp:include>
	<jsp:include page="footerscripts.jsp"></jsp:include>

	<div class="modal fade" id="basic" tabindex="-1" role="basic"
		aria-hidden="true">
		<div class="modal-dialog">
			<!-- BEGIN FORM-->
			<form action="./ConferenceBookingCancelRequest" id="form_sample_11"
				class="form-horizontal" method="post">
				<input type="hidden"
					name="conf_booking_cancel_request_to_employee_id"
					id="conf_booking_cancel_request_to_employee_id" value="1" /> <input
					type="hidden" name="conf_booking_cancel_request_booking_id"
					id="conf_booking_cancel_request_booking_id" value="16" /><input
					type="hidden" name="conf_booking_cancel_request_to_employee_email"
					id="conf_booking_cancel_request_to_employee_email" value="16" /><input
					type="hidden" name="conf_booking_cancel_request_to_employee_name"
					id="conf_booking_cancel_request_to_employee_name" value="16" />
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"
							aria-hidden="true"></button>
						<h4 class="modal-title">
							<b>Booking cancel request</b>
						</h4>
					</div>
					<div class="modal-body">
						<div class="form-body">
							<div class="alert alert-danger display-hide">
								<button class="close" data-close="alert"></button>
								You have some form errors. Please check below.
							</div>
							<div class="alert alert-success display-hide">
								<button class="close" data-close="alert"></button>
								Your form validation is successful!
							</div>

							<div class="form-group">
								<label class="control-label col-md-4">Description <span
									class="required"> * </span>
								</label>
								<div class="col-md-8">
									<textarea class="form-control" type="text"
										name="conf_booking_cancel_request_descr"
										id="conf_booking_cancel_request_descr"></textarea>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn default" data-dismiss="modal">Close</button>
						<button type="submit" class="btn blue">Send request</button>
					</div>
				</div>
			</form>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.modal -->
	<div id="scheduleconfModal" class="modal fade" id="basic" tabindex="-1"
		role="basic" aria-hidden="true">
		<div class="modal-dialog">
			<!-- BEGIN FORM-->
			<form action="./ConferenceBooking" id="form_sample_1"
				class="form-horizontal" method="post">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"
							aria-hidden="true"></button>
						<h4 class="modal-title">
							<b>Schedule Conference</b>
						</h4>
					</div>
					<div class="modal-body">
						<div class="form-body">
							<div class="alert alert-danger display-hide">
								<button class="close" data-close="alert"></button>
								You have some form errors. Please check below.
							</div>
							<div class="alert alert-success display-hide">
								<button class="close" data-close="alert"></button>
								Your form validation is successful!
							</div>
							<div class="form-group">
								<label class="control-label col-md-4">Conference room
									name <span class="required"> * </span>
								</label>
								<div class="col-md-8">
									<Select name="conf_master_id" id="conf_master_id"
										class="form-control">
										<option value="">select...</option>
										<%
											Connection con = doConnection.getConnection();
											PreparedStatement ps = con
													.prepareStatement("select * from conference_master");
											ResultSet rs = ps.executeQuery();
											while (rs.next()) {
										%>
										<option value="<%=rs.getString("conf_master_id")%>"><%=rs.getString("conf_master_name")%></option>
										<%
											}
										%>
									</Select>
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-4">Date <span
									class="required"> * </span>
								</label>
								<div class="col-md-8">
									<input class="form-control date-picker" size="16" type="text"
										name="conf_date" id="conf_date" value="" readonly="readonly" />
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-4">Conference start
									time <span class="required"> * </span>
								</label>
								<div class="col-md-8">
									<input name="conf_booking_timestart"
										id="conf_booking_timestart" type="text"
										class="form-control   timepicker timepicker-no-seconds"
										readonly="readonly" />
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-4">Conference end
									time <span class="required"> * </span>
								</label>
								<div class="col-md-8">
									<input class="form-control  timepicker timepicker-no-seconds"
										type="text" name="conf_booking_timeend"
										id="conf_booking_timeend" value="" readonly="readonly" />
								</div>
							</div>


							<div class="form-group">
								<label class="control-label col-md-4">Conference type <span
									class="required"> * </span>
								</label>
								<div class="col-md-8">
									<select class="form-control" name="conf_booking_type"
										id="conf_booking_type">
										<option value="Employee Conference">Employee Conference</option>
										<option value="Client Conference">Client Conference</option>
									</select>

								</div>
							</div>


							<div class="form-group">
								<label class="control-label col-md-4">Status <span
									class="required"> * </span>
								</label>
								<div class="col-md-8">
									<select class="form-control" name="conf_booking_status"
										id="conf_booking_status">
										<option value="1">Enable</option>
										<option value="0">Disable</option>
									</select>

								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn default" data-dismiss="modal">Close</button>
						<button type="submit" class="btn blue">Schedule</button>
					</div>
				</div>
			</form>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.modal -->

	<div id="fullCalModal" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<span aria-hidden="true">�</span> <span class="sr-only">close</span>
					</button>
					<h4 id="modalTitle" class="modal-title"></h4>
				</div>
				<div id="modalBody" class="modal-body"></div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<a class="btn btn-danger" data-toggle="modal" href="#basic"
						data-dismiss="modal"> Request reschedule </a>
				</div>
			</div>
		</div>
	</div>



</body>
<!-- END BODY -->
</html>
<%
}
else
{
	response.sendRedirect("index.jsp");
}
%>