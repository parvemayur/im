<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.Intranet.Util.doConnection"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<jsp:include page="headerscripts.jsp"></jsp:include>
</head>
<body class="page-header-fixed page-quick-sidebar-over-content ">
	<!-- BEGIN HEADER -->

	<jsp:include page="header.jsp"></jsp:include>
	<!-- END HEADER -->
	<div class="clearfix"></div>
	<!-- BEGIN CONTAINER -->
	<div class="page-container">
		<!-- BEGIN SIDEBAR -->

		<jsp:include page="sidebar.jsp"></jsp:include>
		<!-- END SIDEBAR -->
		<!-- BEGIN CONTENT -->
		<div class="page-content-wrapper">
			<div class="page-content">

				<!-- BEGIN PAGE HEADER-->
				<h3 class="page-title">IT Updates</h3>
				<div class="page-bar">
					<ul class="page-breadcrumb">
						<li><i class="fa fa-home"></i> <a href="index.html">HR</a> <i
							class="fa fa-angle-right"></i></li>
						<li><a href="#">Policy Details</a></li>
					</ul>
				</div>
				<!-- END PAGE HEADER-->

				<!-- BEGIN PAGE CONTENT-->
				<div class="row">
					<div class="col-md-12">
						<!-- BEGIN VALIDATION STATES-->
						<div class="portlet box blue-madison">
							<div class="portlet-title">
								<div class="caption">
									<i class="fa fa-gift"></i>IT POLICY UPDATE
								</div>
							</div>
							<div class="portlet-body form">
								<!-- BEGIN FORM-->
								<form action="./updateitpolicydet" id="form_sample_1"
									class="form-horizontal" method="post">
									<div class="form-body">
										<div class="alert alert-danger display-hide">
											<button class="close" data-close="alert"></button>
											You have some form errors. Please check below.
										</div>
										<div class="alert alert-success display-hide">
											<button class="close" data-close="alert"></button>
											Your form validation is successful!
										</div>
									<div class="form-group">
										<label class="control-label col-md-3">Title <span class="required">
										* </span>
										</label>
										<div class="col-md-4">
											<input type="text" name="ptitle" id="ptitle" data-required="1" class="form-control"/>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3">Details <span class="required">
										* </span>
										</label>
										<div class="col-md-4">
											<input name="pdetails" id="pdetails" type="text" class="form-control"/>
										</div>
									</div>
								<div class="form-group">
										<label class="control-label col-md-3"> Valid From  <span class="required">
										* </span>
										</label>
										<div class="col-md-4">
										<input class="form-control date-picker" size="16" type="text" name="validpfrm" id="validpfrm" value=""/>
								</div>
								</div>
								<div class="form-group">
										<label class="control-label col-md-3"> Valid To  <span class="required">
										* </span>
										</label>
										<div class="col-md-4">
										<input class="form-control date-picker" size="16" type="text" name="validpto" id="validpto" value=""/>
								</div>
								</div>
								
									<div class="form-group">
										<label class="control-label col-md-3">Sorting Order <span class="required">
										* </span>
										</label>
										<div class="col-md-4">
											<input name="sortorder" id="sortorder" type="text" class="form-control"/>
										</div>
									</div>
								
								
									<div class="form-group">
										<label class="control-label col-md-3">Status <span class="required">
										* </span>
										</label>
										<div class="col-md-4">
											<select class="form-control" name="txtstatus"  id="txtstatus" >
												<option value="Enable">Enable</option>
												<option value="Disable">Disable</option>
												</select>
											
										</div>
									</div>
									</div>
									<div class="form-actions">
										<div class="row">
											<div class="col-md-offset-3 col-md-9">
												<button type="submit" class="btn blue-madison">Submit</button>
											</div>
										</div>
									</div>
								</form>
								<!-- END FORM-->
							</div>
						</div>
						<!-- END VALIDATION STATES-->
					</div>
				</div>
				<!-- END PAGE CONTENT-->
				<div class="row">
					<div class="col-md-12">
						<div class="portlet box blue-madison">
							<div class="portlet-title">
								<div class="caption">
									<i class="fa fa-comments"></i>IT POLICY UPDATE
								</div>
							</div>
							<div class="portlet-body">
								<div class="table-scrollable">
									<table class="table table-striped table-hover">
								<thead>
								<tr>
									<th>
										 ID
									</th>
									<th>
										   Name
									</th>
									<th>
										  Details
									</th>
									<th>
										 Valid From
									</th>
									<th>
										 Valid To
									</th>
								</tr>
								</thead>
								<tbody>
								
								<%
								PreparedStatement ps1=null;
			                    ResultSet rs1=null;
			                    Connection con=null;
			                    con=doConnection.getConnection();
			                    ps1=con.prepareStatement("select  it_policies_id,it_policies_name,it_policies_descr,it_policies_sortorder,it_policies_validfrom,it_policies_validto,it_policies_status from it_policies where it_policies_status <> 'Disable' order by it_policies_sortorder; ");
			                    
			                   

			                    System.out.println("ps1=="+ps1);
			                    rs1=ps1.executeQuery();
			                    
			                     while(rs1.next()){   
			                         
			                   
								
								%>
								
								<tr>
									<td>
										<%=rs1.getString(1) %> 
									</td>
									<td>
										 <%=rs1.getString(2) %> 
									</td>
									<td>
										 <%=rs1.getString(3) %> 
									</td>
									<%
												   SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
                                                SimpleDateFormat  sdf2 = new SimpleDateFormat("dd-MM-yyyy");
                                                Date date = null;
                                                date = (Date) sdf1.parse(rs1.getString(5));

                                                String DisplayDate = sdf2.format(date);
                                               
												
												
												%>
									<td>
										 <%=DisplayDate %> 
									</td>
									<%date = (Date) sdf1.parse(rs1.getString(5));
 DisplayDate = sdf2.format(date);
                                    %>
									<td>
										
										<%=DisplayDate %> </span>
									</td>
								</tr>
								 <%
                        
                }%>
								</tbody>
								</table>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- BEGIN PAGE CONTENT-->
			</div>
			<!-- END CONTENT -->
			<!-- BEGIN QUICK SIDEBAR -->

			<jsp:include page="quicksidebar.jsp"></jsp:include>
			<!-- END QUICK SIDEBAR -->
		</div>
		<!-- END CONTAINER -->
		<!-- BEGIN FOOTER -->

		<jsp:include page="footer.jsp"></jsp:include>
		<!-- END FOOTER -->

		<jsp:include page="footerscripts.jsp"></jsp:include>
</body>
<!-- END BODY -->
</html>