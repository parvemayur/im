<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.Intranet.Util.doConnection"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<jsp:include page="headerscripts.jsp"></jsp:include>
</head>
<body class="page-header-fixed page-quick-sidebar-over-content ">
	<!-- BEGIN HEADER -->

	<jsp:include page="header.jsp"></jsp:include>
	<!-- END HEADER -->
	<div class="clearfix"></div>
	<!-- BEGIN CONTAINER -->
	<div class="page-container">
		<!-- BEGIN SIDEBAR -->

		<jsp:include page="sidebar.jsp"></jsp:include>
		<!-- END SIDEBAR -->
		<!-- BEGIN CONTENT -->
		<div class="page-content-wrapper">
			<div class="page-content">

				<!-- BEGIN PAGE HEADER-->
				<h3 class="page-title">Market Place</h3>
				<div class="page-bar">
					<ul class="page-breadcrumb">
						<li><i class="fa fa-home"></i> <a href="index.html"></a> <i
							class="fa fa-angle-right"></i></li>
						<li><a href="#"></a></li>
					</ul>
				</div>
				<!-- END PAGE HEADER-->

				<!-- BEGIN PAGE CONTENT-->
				<div class="row">
					<div class="col-md-12">
						<form class="form-horizontal form-row-seperated" action="#">
							<div class="portlet">
								<div class="portlet-title">
									<div class="caption">
										<i class="fa fa-shopping-cart"></i>Product Details
									</div>
									
									<%String msg=request.getParameter("msg"); %>
									<%if(msg!=null){ %>
									<strong><%=msg %></strong>
									<%} %>
									<div class="actions btn-set">
										
										
										
										<a href="market_advert.jsp"><button id="genpassword" class="btn btn-success" type="button"><i class="fa fa-arrow-left fa-fw"></i>New Entry</button></a>

									</div>
								</div>
								<div class="portlet-body">


									<div class="tab-content no-space">


										<div class="row">
											<div id="tab_images_uploader_filelist"
												class="col-md-6 col-sm-12"></div>
										</div>
										<table class="table table-bordered table-hover">
											<thead>
												<tr role="row" class="heading">
													<th width="10%">Image</th>
													<th width="10%">Title</th>
													<th width="25%">Category</th>
													<th width="10%">Price</th>
													<th width="10%">Description</th>
													<th width="10%">Owners Info(Emp id)</th>
												</tr>
													</thead>
											<tbody>
											
											
											<%
											Connection con=null;
											PreparedStatement ps=null,ps1=null;
											ResultSet rs=null,rs1=null;
											try{
												
											con=doConnection.getConnection();
													
											ps=con.prepareStatement("select * from ecommerce_advt ");
											rs=ps.executeQuery();
											while(rs.next())
											{
												
											ps1=con.prepareStatement("select ");
											
											%>
												<tr>
													<td> <img class="img-responsive" src="/assets/admin/pages/media/works/bg-opacity.png" alt="">													</td>
													<td><%=rs.getString("ecommerce_title")%></td>
													<td><%=rs.getString("ecommerce_category")%></td>
													<td><%=rs.getString("ecommerce_price")%></td>
													<td><%=rs.getString("ecommerce_addescription")%></td>

													<td><%=rs.getString("ecommerce_empid")%></td>


												</tr>
												
												<%}
											}catch(Exception e){
											e.printStackTrace();
											}%>
																							</tbody>
										</table>
									</div>


								</div>
							</div>
					
					</form>
				</div>
			</div>

			<!-- END CONTENT -->
			<!-- BEGIN QUICK SIDEBAR -->

			<jsp:include page="quicksidebar.jsp"></jsp:include>
			<!-- END QUICK SIDEBAR -->
		</div>
		<!-- END CONTAINER -->
		<!-- BEGIN FOOTER -->

		<jsp:include page="footer.jsp"></jsp:include>
		<!-- END FOOTER -->

		<jsp:include page="footerscripts.jsp"></jsp:include>
</body>
<!-- END BODY -->
</html>