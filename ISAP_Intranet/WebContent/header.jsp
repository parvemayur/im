<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="com.Intranet.Util.doConnection"%>
<%@page import="java.sql.Connection"%>
<div class="page-header navbar navbar-fixed-top">
	<!-- BEGIN HEADER INNER -->
	<div class="page-header-inner">
		<!-- BEGIN LOGO -->
		<div class="page-logo">
			<h4 style="color:#fff">INDO SCHOTTLE</h4>
		</div>
		<!-- END LOGO -->
		<!-- BEGIN RESPONSIVE MENU TOGGLER -->
		<a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
		</a>
		<!-- END RESPONSIVE MENU TOGGLER -->
		<!-- BEGIN TOP NAVIGATION MENU -->
		<div class="top-menu">
			<ul class="nav navbar-nav pull-right">
				<!-- BEGIN NOTIFICATION DROPDOWN -->
				<li class="dropdown dropdown-extended dropdown-inbox" id="header_inbox_bar">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
					<i class="fa fa-bullhorn"></i>
					<span class="badge badge-default">
					<%
											try {
												Connection con = doConnection.getConnection();
												PreparedStatement ps = con
														.prepareStatement("select count(*) from conf_booking_cancel_request where conf_booking_cancel_request_status=0 and conf_booking_cancel_request_to_employee_id=?");
												ps.setInt(1,Integer.parseInt(""+session.getAttribute("userid")));
												ResultSet rs = ps.executeQuery();
												while (rs.next()) {
										%>
					
					<%=rs.getString(1) %>
					<%
											}
											} catch (Exception e) {
												e.printStackTrace();
											}
										%>
					</span>
					</a>
					<ul class="dropdown-menu">
						<li class="external">
							<h3><span class="bold">Conference Reschedule Notifications</span></h3>
							<a href="conference_notification.jsp">view all</a>
						</li>
						<li>
							<ul class="dropdown-menu-list scroller" style="height: 275px;" data-handle-color="#637283">
								<%
											try {
												Connection con = doConnection.getConnection();
												PreparedStatement ps = con
														//.prepareStatement("select em.employee_master_name,cb.conf_booking_type,cbcr.conf_booking_cancel_request_id,cbcr.conf_booking_cancel_request_booking_id,cbcr.conf_booking_cancel_request_from_employee_id,cm.conf_master_name,CONVERT(VARCHAR(24),cb.conf_booking_timestart,113) as conf_booking_timestart,CONVERT(VARCHAR(24),cb.conf_booking_timeend,113) as conf_booking_timeend,cbcr.conf_booking_cancel_request_from_employee_id,cbcr.conf_booking_cancel_request_descr from conf_booking_cancel_request cbcr,conference_booking cb,conference_master cm,employee_master em where cbcr.conf_booking_cancel_request_booking_id=cb.conf_booking_id and cb.conf_master_id=cm.conf_master_id and cbcr.conf_booking_cancel_request_status=0 and cbcr.conf_booking_cancel_request_from_employee_id=em.employee_master_id and CAST(cb.conf_booking_timestart AS DATE)>=CAST(getDate() AS DATE) and cbcr.conf_booking_cancel_request_status=0 and cbcr.conf_booking_cancel_request_to_employee_id=?");
														.prepareStatement("select em.name,cb.conf_booking_type,cbcr.conf_booking_cancel_request_id,cbcr.conf_booking_cancel_request_booking_id,cbcr.conf_booking_cancel_request_from_employee_id,cm.conf_master_name,CONVERT(VARCHAR(24),cb.conf_booking_timestart,113) as conf_booking_timestart,CONVERT(VARCHAR(24),cb.conf_booking_timeend,113) as conf_booking_timeend,cbcr.conf_booking_cancel_request_from_employee_id,cbcr.conf_booking_cancel_request_descr from conf_booking_cancel_request cbcr,conference_booking cb,conference_master cm,employee_master em where cbcr.conf_booking_cancel_request_booking_id=cb.conf_booking_id and cb.conf_master_id=cm.conf_master_id and cbcr.conf_booking_cancel_request_status=0 and cbcr.conf_booking_cancel_request_from_employee_id=em.empcode and CAST(cb.conf_booking_timestart AS DATE)>=CAST(getDate() AS DATE) and cbcr.conf_booking_cancel_request_status=0 and cbcr.conf_booking_cancel_request_to_employee_id=?");
												ps.setInt(1,Integer.parseInt(""+session.getAttribute("userid")));
												ResultSet rs = ps.executeQuery();
												while (rs.next()) {
										%>
								<li>
									<a href="conference_notification.jsp">
									<span class="photo">
									<img src="assets/admin/layout3/img/avatar.png" class="img-circle" alt="">
									</span>
									<span class="subject">
									<span class="from">
									<%=rs.getString("name")%> </span>
									<span class="time"> </span>
									</span>
									<span class="message">
									<b><%=rs.getString("conf_booking_type")%></b><br/>
									<b><%=rs.getString("conf_booking_timestart")%> - <%=rs.getString("conf_booking_timeend")%></b><br/>
									<%=rs.getString("conf_booking_cancel_request_descr")%> </span>
									</a>
								</li>
								<%
											}
											} catch (Exception e) {
												e.printStackTrace();
											}
										%>
										</ul>
						</li>
					</ul>
				</li>
				<!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
				<li class="dropdown dropdown-extended dropdown-notification" id="header_notification_bar">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
					<i class="fa fa-gift"></i>
					<span class="badge badge-default">
				<%
				PreparedStatement ps2=null;
				ResultSet rs2=null;
				Connection con=doConnection.getConnection();
				ps2=con.prepareStatement("select count(*) from EMPLOYEE_MASTER where DATEPART(dd,datebirth)= DATEPART(dd,GETDATE()) and DATEPART(MM,datebirth)= DATEPART(MM,GETDATE())");
				rs2=ps2.executeQuery();
				while(rs2.next())
				{
					
				
				%>
				<%=rs2.getString(1) %>
				<%} %>
				
				 </span>
					</a>
					<ul class="dropdown-menu">
						<li class="external">
							<h3><span class="bold">Birthday's Notifications </span></h3>
							<a href="#">view all</a>
						</li>
						<li>
							<ul class="dropdown-menu-list scroller" style="height: 250px;" data-handle-color="#637283">
								<%
								PreparedStatement ps1=null,ps3=null;
								ResultSet rs1=null,rs3=null;
								//Connection con=doConnection.getConnection();
								ps1=con.prepareStatement("select name,department,DATEBIRTH,DATEPART(DD,datebirth)as dayno,DATEPART(MM,datebirth) as MonthNo,* from EMPLOYEE_MASTER  where  DATEPART(dd,datebirth)= DATEPART(dd,GETDATE()) and DATEPART(MM,datebirth)= DATEPART(MM,GETDATE()) ");
								rs1=ps1.executeQuery();
								while(rs1.next())
								{
									ps3=con.prepareStatement("select name from dept_master where code=?");
									ps3.setString(1,rs1.getString(2));
									rs3=ps3.executeQuery();
									while(rs3.next())
									{
										
									
								
								%>
								
								
								
								<li>
									<a href="javascript:;">
									<span class="time">Today</span>
									<span class="details">
									<span class="label label-sm label-icon label-success">
									<i class="fa fa-plus">  </i>
									</span>
									 </span>
									 <%=rs1.getString(1) %> </br>
									<strong> Department : </strong><%=rs3.getString(1) %>
									</a>
								</li>
								<%}} %>
							</ul>
						</li>
					</ul>
				</li>
				<!-- END NOTIFICATION DROPDOWN -->
				
				<!-- BEGIN NOTIFICATION DROPDOWN -->
				<!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
				<li class="dropdown dropdown-extended dropdown-notification" id="header_notification_bar">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
					<i class="fa fa-child"></i>
					<span class="badge badge-default">
					<%
				PreparedStatement ps12=null;
				ResultSet rs12=null;
				//Connection con=doConnection.getConnection();
				ps12=con.prepareStatement("select count(*)  from EMPLOYEE_MASTER where DATEPART(MM,datejoin)= DATEPART(MM,GETDATE()) and DATEPART(yyyy,datejoin) = DATEPART(yyyy,GETDATE()) ");
				rs12=ps12.executeQuery();
				while(rs12.next())
				{
					
				
				%>
				<%=rs12.getString(1) %>
				<%} %>
				</span>
					</a>
					<ul class="dropdown-menu">
						<li class="external">
							<h3><span class="bold">New Joiners Notifications </span></h3>
							<a href="extra_profile.html">view all</a>
						</li>
						
								
						<%
								PreparedStatement ps10=null,ps11=null;
								ResultSet rs10=null,rs11=null;
								//Connection con=doConnection.getConnection();
								ps10=con.prepareStatement("select NAME,DEPARTMENT,DATEBIRTH,DATEPART(DD,datejoin)as dayno,DATEPART(MM,datejoin) as MonthNo,DATEPART(yyyy,datejoin) as YearNo,city from EMPLOYEE_MASTER where DATEPART(MM,datejoin)= DATEPART(MM,GETDATE()) and DATEPART(yyyy,datejoin) = DATEPART(yyyy,GETDATE()) ");
								rs10=ps10.executeQuery();
								while(rs10.next())
								{
									ps11=con.prepareStatement("select name from dept_master where code=?");
									ps11.setString(1,rs10.getString(2));
									rs11=ps11.executeQuery();
									while(rs11.next())
									{
										
									
								
								%>
						<li>
							<ul class="dropdown-menu-list scroller" style="height: 250px;" data-handle-color="#637283">
								<li>
									<a href="javascript:;">
									<span class="time">This Month</span>
									<span class="details">
									<span class="label label-sm label-icon label-success">
									<i class="fa fa-plus"></i>
									</span>
								<%=rs10.getString(1) %> </span></br>
									<strong> Department : </strong><%=rs11.getString(1) %>
									</br>
									<strong> From City : </strong><%=rs10.getString(7) %>
									</a>
								</li>
								
							</ul>
						</li>
						<%}
								}%>
					</ul>
				</li>
				<!-- END NOTIFICATION DROPDOWN -->
				
				
				
				
				
				<!-- BEGIN INBOX DROPDOWN -->
				<!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
				<li class="dropdown dropdown-extended dropdown-inbox" id="header_inbox_bar">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
					<i class="icon-envelope-open"></i>
					<span class="badge badge-default">
					<%
				PreparedStatement ps13=null;
				ResultSet rs13=null;
				//Connection con=doConnection.getConnection();
				ps13=con.prepareStatement("select count(*)  from EMPLOYEE_MASTER where DATEPART(MM,datejoin)= DATEPART(MM,GETDATE()) and DATEPART(yyyy,datejoin) = DATEPART(yyyy,GETDATE()) ");
				rs13=ps13.executeQuery();
				while(rs13.next())
				{
					
				
				%>
				<%=rs13.getString(1) %>
				<%} %> </span>
					</a>
					<ul class="dropdown-menu">
						<li class="external">
							<h3>You have <span class="bold">HR & Policy Update Notifications</span> </h3>
							<a href="page_inbox.html">view all</a>
						</li>
						<li>
							<ul class="dropdown-menu-list scroller" style="height: 275px;" data-handle-color="#637283">
								<li>
									<a href="inbox.html?a=view">
									<span class="photo">
									<img src="assets/admin/layout3/img/avatar2.jpg" class="img-circle" alt="">
									</span>
									<span class="subject">
									<span class="from">
									Lisa Wong </span>
									<span class="time">Just Now </span>
									</span>
									<span class="message">
									Vivamus sed auctor nibh congue nibh. auctor nibh auctor nibh... </span>
									</a>
								</li>
										</ul>
						</li>
					</ul>
				</li>
				<!-- END INBOX DROPDOWN -->
				<!-- BEGIN TODO DROPDOWN -->
				<!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
				<li class="dropdown dropdown-extended dropdown-tasks" id="header_task_bar">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
					<i class="icon-calendar"></i>
					<span class="badge badge-default">
					3 </span>
					</a>
					<ul class="dropdown-menu extended tasks">
						<li class="external">
							<h3><span class="bold">Job Openings Notifications</span></h3>
							<a href="page_todo.html">view all</a>
						</li>
						<li>
							<ul class="dropdown-menu-list scroller" style="height: 275px;" data-handle-color="#637283">
								<li>
									<a href="javascript:;">
									<span class="task">
									<span class="desc">New release v1.2 </span>
									<span class="percent">30%</span>
									</span>
									<span class="progress">
									<span style="width: 40%;" class="progress-bar progress-bar-success" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100"><span class="sr-only">40% Complete</span></span>
									</span>
									</a>
								</li>
									</ul>
						</li>
					</ul>
				</li>
				<!-- END TODO DROPDOWN -->
				
				
				<!-- BEGIN TODO DROPDOWN -->
				<!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
				<li class="dropdown dropdown-extended dropdown-tasks" id="header_task_bar">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
					<i class="fa fa-life-ring"></i>
					<span class="badge badge-default">
					<%
				PreparedStatement ps14=null;
				ResultSet rs14=null;
				//Connection con=doConnection.getConnection();
				ps14=con.prepareStatement("select count(*)  from ecommerce_advt where ecommerce_status not like  'published' and DATEPART(MM,ecommerce_advtdate)= DATEPART(MM,GETDATE()) and DATEPART(yyyy,ecommerce_advtdate) = DATEPART(yyyy,GETDATE());");
				rs14=ps14.executeQuery();
				while(rs14.next())
				{
					
				
				%>
				<%=rs14.getString(1) %>
				<%} %> </span>
					</a>
					<ul class="dropdown-menu extended tasks">
						<li class="external">
							<h3> <span class="bold">Market Place Notifications</span></h3>
							<a href="page_todo.html">view all</a>
						</li>
						<li>
							<ul class="dropdown-menu-list scroller" style="height: 275px;" data-handle-color="#637283">
								
								<%
				PreparedStatement ps15=null;
				ResultSet rs15=null;
				//Connection con=doConnection.getConnection();
				ps15=con.prepareStatement("select * from ecommerce_advt where ecommerce_status not like  'published' and DATEPART(MM,ecommerce_advtdate)= DATEPART(MM,GETDATE()) and DATEPART(yyyy,ecommerce_advtdate) = DATEPART(yyyy,GETDATE());");
				rs15=ps15.executeQuery();
				while(rs15.next())
				{
					
				
				%>
								
								<li>
									<a href="javascript:;">
									<span class="task">
									<span class="desc"><strong><%=rs15.getString("ecommerce_title") %></strong>
									 </span>
									 <br/>
									 
									<span class="desc"><%=rs15.getString("ecommerce_addescription") %></span>
									</span>
									<br/>
									<span class="desc"><%=rs15.getString("ecommerce_price") %></span>
									</span>
									
									</a>
								</li>
								<%} %>
									</ul>
						</li>
					</ul>
				</li>
				<!-- END TODO DROPDOWN -->
				
				<!-- BEGIN USER LOGIN DROPDOWN -->
				<!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
				<li class="dropdown dropdown-user">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
					<img alt="" class="img-circle" src="assets/admin/layout/img/avatar3_small.jpg"/>
					<span class="username username-hide-on-mobile">
					<%=session.getAttribute("username") %> </span>
					<i class="fa fa-angle-down"></i>
					</a>
					
					 <%! String LoginID;%>
					<%
					
					LoginID=(String) session.getAttribute("username");
					
					System.out.println("LoginID="+LoginID);%>
					<ul class="dropdown-menu dropdown-menu-default">
						<li>
							<a href="extra_profile.html">
							<i class="icon-user"></i> My Profile </a>
						</li>
						<li>
							<a href="page_calendar.html">
							<i class="icon-calendar"></i> My Calendar </a>
						</li>
						<li>
							<a href="inbox.html">
							<i class="icon-envelope-open"></i> My Inbox <span class="badge badge-danger">
							3 </span>
							</a>
						</li>
						<li>
							<a href="page_todo.html">
							<i class="icon-rocket"></i> My Tasks <span class="badge badge-success">
							7 </span>
							</a>
						</li>
						<li class="divider">
						</li>
						<li>
							<a href="extra_lock.html">
							<i class="icon-lock"></i> Lock Screen </a>
						</li>
						<li>
							<a href="index.jsp">
							<i class="icon-key"></i> Log Out </a>
						</li>
					</ul>
				</li>
				<!-- END USER LOGIN DROPDOWN -->
				<!-- BEGIN QUICK SIDEBAR TOGGLER -->
				<!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
				<!-- <li class="dropdown dropdown-quick-sidebar-toggler">
					<a href="javascript:;" class="dropdown-toggle">
					<i class="icon-logout"></i>
					</a>
				</li>  -->
				<!-- END QUICK SIDEBAR TOGGLER -->
			</ul>
		</div>
		<!-- END TOP NAVIGATION MENU -->
	</div>
	<!-- END HEADER INNER -->
</div>