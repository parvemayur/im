<div class="page-sidebar-wrapper">
	<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
	<!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
	<div class="page-sidebar navbar-collapse collapse">
		<!-- BEGIN SIDEBAR MENU -->
		<!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
		<!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
		<!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
		<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
		<!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
		<!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
		<ul class="page-sidebar-menu" data-keep-expanded="false"
			data-auto-scroll="true" data-slide-speed="200">
			<!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
			<li><img
						src="assets/admin/layout/img/indo-schottle.jpg" alt="logo" style="width:100%"
						 />
					</a>
			</li>
			<li class="sidebar-toggler-wrapper"><!-- BEGIN SIDEBAR TOGGLER BUTTON -->
					<div class="sidebar-toggler">
					</div>
					<!-- END SIDEBAR TOGGLER BUTTON -->
				</li>
				<!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
			
				<li class="start ">
					<a href="dashboard.jsp">
					<i class="icon-home"></i>
					<span class="title">Home</span>
					</a>
					
				</li>
				<li>
					<a href="hr_home.jsp">
					<i class="icon-user"></i>
					<span class="title">HR</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub-menu">
						
						<li>
							<a href="hr_home.jsp">
							<i class="icon-home"></i>
							HR home</a>
						</li>
						
						<li>
							<a href="hr_policies.jsp">
							<i class="icon-handbag"></i>
							HR Policies</a>
						</li>
						<li>
							<a href="hr_updates.jsp">
							<i class="icon-pencil"></i>
							HR updates</a>
						</li>
						
						<li>
							<a href="it_updates.jsp">
							<i class="icon-pencil"></i>
							IT updates</a>
						</li>
						<li>
							<a href="it_updates.jsp">
							<i class="icon-pencil"></i>
							Attendance</a>
						</li>
						<li>
							<a href="it_updates.jsp">
							<i class="icon-pencil"></i>
							Leave Apply</a>
						</li>
						<li>
							<a href="it_updates.jsp">
							<i class="icon-pencil"></i>
							OD Apply</a>
						</li>
						<li>
							<a href="it_updates.jsp">
							<i class="icon-pencil"></i>
						HR Services</a>
						</li>
						<li>
							<a href="#">
							<i class="icon-handbag"></i>
						Masters</a>
						<ul class="sub-menu">
						<li>
						
						<a href="chairmans_msg.jsp">
							<i class="icon-pencil"></i>
						Chairman Message</a>
						</li>
						<li>
						
						<a href="#">
							<i class="icon-pencil"></i>
						Job Opening</a>
						</li>
						<li>
						
						<a href="#">
							<i class="icon-pencil"></i>
						Achievers</a>
						</li>
						
						
						<li>
						
						<a href="#">
							<i class="icon-pencil"></i>
						Service Entry</a>
						</li>
						<li>
						
						<a href="#">
							<i class="icon-pencil"></i>
						Plant Entry</a>
						</li>
						<li>
						
						<a href="#">
							<i class="icon-pencil"></i>
						Work Flow Entry</a>
						</li>
						<li>
						
						<a href="#">
							<i class="icon-pencil"></i>
						Service Allocation Entry</a>
						</li>
						</ul>
						</li>
						
						
						
						
						
					</ul>
				</li>
				<li>
					<a href="javascript:;">
					<i class="fa fa-building"></i>
					<span class="title">Conference Room</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub-menu">
						<li>
							<a href="conference_master.jsp">
							<i class="icon-home"></i>
							Conference Master</a>
						</li>
						<li>
							<a href="conference_booking.jsp">
							<i class="icon-home"></i>
							Booking</a>
						</li>
						<li>
							<a href="conference_notification.jsp">
							<i class="icon-home"></i>
							Notifications</a>
						</li>
						
					</ul>
				</li>
				<li>
					<a href="javascript:;">
					<i class="fa fa-fax"></i>
					<span class="title"> eRoom</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub-menu">
						<li>
							<a href="#">
							<i class="icon-home"></i>
							MRM</a>
						</li>
						<li>
							<a href="#">
							<i class="icon-home"></i>
							SC</a>
						</li>
						
					</ul>
				</li>
				
				<li>
					<a href="javascript:;">
					<i class="fa  fa-deviantart"></i>
					<span class="title">Project</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub-menu">
						<li>
							<a href="#">
							<i class="icon-home"></i>
						RFQ</a>
						</li>
						
						
					</ul>
				</li>
				<li>
					<a href="javascript:;">
					<i class="fa fa-paper-plane-o"></i>
					<span class="title">Travel Management</span>
					<span class="arrow "></span>
					</a>
					
				</li>
				<li>
					<a href="javascript:;">
					<i class="fa fa-file-text-o"></i>
					<span class="title">Service Request</span>
					<span class="arrow "></span>
					</a>
					
				</li>
				<li>
					<a href="javascript:;">
					<i class="icon-basket"></i>
					<span class="title">MarketPlace</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub-menu">
						<li>
						
							<a href="ecommerce_product_list.jsp">
							<i class="icon-home"></i>
							
							Dashboard</a>
						</li>
						
					</ul>
				</li>
				
				<!-- BEGIN ANGULARJS LINK -->
				
				<!-- END ANGULARJS LINK -->
				</ul>
			<!-- END SIDEBAR MENU -->
		</div>
	</div>