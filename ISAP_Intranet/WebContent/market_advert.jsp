<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.Intranet.Util.doConnection"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<jsp:include page="headerscripts.jsp"></jsp:include>
</head>
<body class="page-header-fixed page-quick-sidebar-over-content ">
	<!-- BEGIN HEADER -->

	<jsp:include page="header.jsp"></jsp:include>
	<!-- END HEADER -->
	<div class="clearfix"></div>
	<!-- BEGIN CONTAINER -->
	<div class="page-container">
		<!-- BEGIN SIDEBAR -->

		<jsp:include page="sidebar.jsp"></jsp:include>
		<!-- END SIDEBAR -->
		<!-- BEGIN CONTENT -->
		<div class="page-content-wrapper">
			<div class="page-content">

				<!-- BEGIN PAGE HEADER-->
				<h3 class="page-title">Market Place</h3>
				<div class="page-bar">
					<ul class="page-breadcrumb">
						<li><i class="fa fa-home"></i> <a href="index.html"></a> <i
							class="fa fa-angle-right"></i></li>
						<li><a href="#"></a></li>
					</ul>
				</div>
				<!-- END PAGE HEADER-->

				<!-- BEGIN PAGE CONTENT-->
				<div class="row">
					<div class="col-md-12">
						<!-- BEGIN VALIDATION STATES-->
						<div class="portlet box blue-madison">
							<div class="portlet-title">
								<div class="caption">
									<i class="fa fa-gift"></i>Sale your product here
								</div>
							</div>
							<div class="portlet-body form">
								<!-- BEGIN FORM-->
								<form action="./UploadMarketAdd" id="form_sample_1"
									class="form-horizontal" method="post" enctype="multipart/form-data">
									<div class="form-body">
										<div class="alert alert-danger display-hide">
											<button class="close" data-close="alert"></button>
											You have some form errors. Please check below.
										</div>
										<div class="alert alert-success display-hide">
											<button class="close" data-close="alert"></button>
											Your form validation is successful!
										</div>
										 <%! String LoginID;%>
										<%
										
										LoginID=(String) session.getAttribute("userid");
										
										System.out.println("LoginID="+LoginID);%>
										
										<input type="hidden" name="LoginID" id="LoginID"
													data-required="1" class="form-control" value="<%=LoginID%>" />
										<div class="form-group">
											<label class="control-label col-md-3">Ad Title <span
												class="required"> * </span>
											</label>
											<div class="col-md-4">
												<input type="text" name="adtitle" id="adtitle"
													data-required="1" class="form-control" />
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-md-3">Category
												<span class="required"> * </span>
											</label>
											<div class="col-md-4">
												<select class="form-control" name="txtcat" id="txtcat">
													<option value="Mobile and Tablets">Mobile and Tablets</option>
													<option value="Electronics and Computers">Electronics and Computers</option>
													<option value="Vehicles">Vehicles</option>
													<option value="Home and Furniture">Home and Furniture</option>
													<option value="Animals">Animals</option>
													<option value="Books,Sports and Hobbies">Books,Sports and Hobbies</option>
													<option value="Services">Services</option>
													<option value="Job">Job</option>
													<option value="Real Estate">Real Estate</option>
													
												</select>
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-md-3">Price
												<span class="required"> * </span>
											</label>
											<div class="col-md-4">
												<input class="form-control" size="16"
													type="text" name="txtprice" id="txtprice"  />
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-md-3">Ad Description
												<span class="required"> * </span>
											</label>
											<div class="col-md-4">
												<input class="form-control" size="16"
													type="text" name="addescrip" id="addescrip" value="" />
											</div>

										</div>

										<div class="form-group">
											<label class="control-label col-md-3">Upload Images <span
												class="required"> * </span>
											</label>
											<div class="col-md-4">
												<input name="image1" id="image1" type="file"
													class="form-control" />
											</div>
										</div>


												

										
									</div>
									<div class="form-actions">
										<div class="row">
											<div class="col-md-offset-3 col-md-9">
												<button type="submit" class="btn blue-madison">Submit</button>
											</div>
										</div>
									</div>
								</form>
								<!-- END FORM-->
							</div>
						</div>
						<!-- END VALIDATION STATES-->
					</div>
				</div>
				<!-- END PAGE CONTENT-->
								<!-- BEGIN PAGE CONTENT-->
			</div>
			<!-- END CONTENT -->
			<!-- BEGIN QUICK SIDEBAR -->

			<jsp:include page="quicksidebar.jsp"></jsp:include>
			<!-- END QUICK SIDEBAR -->
		</div>
		<!-- END CONTAINER -->
		<!-- BEGIN FOOTER -->

		<jsp:include page="footer.jsp"></jsp:include>
		<!-- END FOOTER -->

		<jsp:include page="footerscripts.jsp"></jsp:include>
</body>
<!-- END BODY -->
</html>