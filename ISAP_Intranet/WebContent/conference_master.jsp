<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="com.Intranet.Util.doConnection"%>
<%@page import="java.sql.Connection"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<jsp:include page="headerscripts.jsp"></jsp:include>
</head>
<body class="page-header-fixed page-quick-sidebar-over-content ">
	<!-- BEGIN HEADER -->

	<jsp:include page="header.jsp"></jsp:include>
	<!-- END HEADER -->
	<div class="clearfix"></div>
	<!-- BEGIN CONTAINER -->
	<div class="page-container">
		<!-- BEGIN SIDEBAR -->

		<jsp:include page="sidebar.jsp"></jsp:include>
		<!-- END SIDEBAR -->
		<!-- BEGIN CONTENT -->
		<div class="page-content-wrapper">
			<div class="page-content">

				<!-- BEGIN PAGE HEADER-->
				<h3 class="page-title">Conference Master</h3>
				<div class="page-bar">
					<ul class="page-breadcrumb">
						<li><i class="fa fa-home"></i> <a href="index.html">Conference</a>
							<i class="fa fa-angle-right"></i></li>
						<li><a href="#">Conference Master</a></li>
					</ul>
				</div>
				<%
					if (session.getAttribute("alert") != null) {
				%>
				<div class="row">
					<div class="col-md-12">
						<div class="alert alert-danger">
							<button class="close" data-close="alert"></button>
							<%=session.getAttribute("alert")%>
						</div>
					</div>
				</div>
				<%
					session.removeAttribute("alert");
					}
				%>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<div class="row">
					<div class="col-md-12">
						<!-- BEGIN VALIDATION STATES-->
						<div class="portlet box blue-madison">
							<div class="portlet-title">
								<div class="caption">
									<i class="fa fa-gift"></i>Conference Master
								</div>
							</div>
							<div class="portlet-body form">
								<!-- BEGIN FORM-->
								<form action="./ConferenceMaster" id="form_sample_1"
									class="form-horizontal" method="post">
									<div class="form-body">
										<div class="alert alert-danger display-hide">
											<button class="close" data-close="alert"></button>
											You have some form errors. Please check below.
										</div>
										<div class="alert alert-success display-hide">
											<button class="close" data-close="alert"></button>
											Your form validation is successful!
										</div>
										<div class="form-group">
											<label class="control-label col-md-3">Name <span
												class="required"> * </span>
											</label>
											<div class="col-md-4">
												<input type="text" name="conf_master_name"
													id="conf_master_name" class="form-control" />
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-md-3">Plant <span
												class="required"> * </span>
											</label>
											<div class="col-md-4">
												<input name="conf_master_plant" id="conf_master_plant"
													type="text" class="form-control" />
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-md-3">Seating
												Capacity <span class="required"> * </span>
											</label>
											<div class="col-md-4">
												<input class="form-control" type="text"
													name="conf_master_seatcount" id="conf_master_seatcount"
													value="" />
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-md-3">Description <span
												class="required"> * </span>
											</label>
											<div class="col-md-4">
												<input class="form-control" type="text"
													name="conf_master_descr" id="conf_master_descr" value="" />
											</div>
										</div>


										<div class="form-group">
											<label class="control-label col-md-3">Status <span
												class="required"> * </span>
											</label>
											<div class="col-md-4">
												<select class="form-control" name="conf_master_status"
													id="conf_master_status">
													<option value="1">Enable</option>
													<option value="0">Disable</option>
												</select>

											</div>
										</div>
									</div>
									<div class="form-actions">
										<div class="row">
											<div class="col-md-offset-3 col-md-9">
												<button type="submit" class="btn blue-madison">Submit</button>
											</div>
										</div>
									</div>
								</form>
								<!-- END FORM-->
							</div>
						</div>
						<!-- END VALIDATION STATES-->
					</div>
				</div>
				<!-- END PAGE CONTENT-->

				<div class="row">
					<div class="col-md-12">
						<div class="portlet box blue-madison">
							<div class="portlet-title">
								<div class="caption">
									<i class="fa fa-globe"></i>Conference Master List
								</div>
								<div class="tools">
									<a href="javascript:;" class="collapse"> </a> <a
										href="#portlet-config" data-toggle="modal" class="config">
									</a> <a href="javascript:;" class="reload"> </a> <a
										href="javascript:;" class="remove"> </a>
								</div>
							</div>
							<div class="portlet-body">
								<table class="table table-striped table-bordered table-hover"
									id="sample_6">
									<thead>
										<tr>
											<th>Name</th>
											<th>Plant</th>
											<th>Seating capacity</th>
											<th>Description</th>
											<th>Status</th>
										</tr>
									</thead>
									<tbody>
										<%
											Connection con = doConnection.getConnection();
											PreparedStatement ps = con
													.prepareStatement("select * from conference_master");
											ResultSet rs = ps.executeQuery();
											while (rs.next()) {
										%>
										<tr>
											<td><%=rs.getString("conf_master_name")%></td>
											<td><%=rs.getString("conf_master_plant")%></td>
											<td><%=rs.getString("conf_master_seatcount")%></td>
											<td><%=rs.getString("conf_master_descr")%></td>
											<td><%=rs.getString("conf_master_status")%></td>
										</tr>
										<%
											}
										%>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- END CONTENT -->
		<!-- BEGIN QUICK SIDEBAR -->

		<jsp:include page="quicksidebar.jsp"></jsp:include>
		<!-- END QUICK SIDEBAR -->
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->

	<jsp:include page="footer.jsp"></jsp:include>
	<!-- END FOOTER -->

	<jsp:include page="footerscripts.jsp"></jsp:include>
</body>
<!-- END BODY -->
</html>