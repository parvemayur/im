<%@page import="java.text.Format"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.ResultSet"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="com.Intranet.Util.doConnection"%>
<%@page import="java.sql.Connection"%>
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<jsp:include page="headerscripts.jsp"></jsp:include>
</head>
<body class="page-header-fixed page-quick-sidebar-over-content ">
	<!-- BEGIN HEADER -->

	<jsp:include page="header.jsp"></jsp:include>
	<!-- END HEADER -->
	<div class="clearfix"></div>
	<!-- BEGIN CONTAINER -->
	<div class="page-container">
		<!-- BEGIN SIDEBAR -->
		<%
Connection con=null;
con=doConnection.getConnection();
		
		%>
		<jsp:include page="sidebar.jsp"></jsp:include>
		<!-- END SIDEBAR -->
		<!-- BEGIN CONTENT -->
		<div class="page-content-wrapper">
			<div class="page-content">
				
				<!-- BEGIN PAGE HEADER-->
				<h3 class="page-title">My Work Space</h3>
				<!-- <div class="page-bar">
					<ul class="page-breadcrumb">
						<li><i class="fa fa-home"></i> <a href="index.html">Home</a>
							<i class="fa fa-angle-right"></i></li>
						
					</ul>
					
				</div> -->
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<div class="row">
					<div class="col-md-12">
						<ul class="media-list note note-warning">

						<%
						 DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                        DateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy");
                        Date date1 = new Date();
                        Date date2 = new Date();
                        System.out.println(dateFormat.format(date1));
                        String date11 = dateFormat.format(date2);
                        System.out.println("date11=" + date11);

                        Format formatter = new SimpleDateFormat("MM");
                        String s2 = formatter.format(date1);
                        System.out.println(s2);
                        Format formatter1 = new SimpleDateFormat("yyyy");
                        String s1 = formatter1.format(date1);
                        System.out.println(s1);

						PreparedStatement ps5=null;
						ResultSet rs5=null;
						ps5=con.prepareStatement("select * from chairman_msg where  chairman_msg_status is not null and chairman_msg_year=?" );
						
						
						
						ps5.setString(1, s1);
						rs5=ps5.executeQuery();
						while(rs5.next())
						{
						
						%>
							<li class="media">
								<div class="pull-left text-center">
									<img class="media-object"
										src="assets/admin/layout/img/chairman.png" alt="">
						
									<h5 class="media-heading">
										<b>Mr. Vijay Pusalkar</b><br />(Chairman)
									</h5>

								</div>
								<div class="media-body">
									<p><%=rs5.getString("chairman_msg_msgname") %></p>
									<!-- Nested media object -->


								</div>
							</li>
							<%} %>

						</ul>
					</div>
				</div>

				<div class="row">
					<div class="col-md-12">
						<div class="note note-info">
							<h4>
								<b><i class="icon-badge"></i> Achievers</b>
							</h4>
							<p>
								<br />
							<ul class="chats">
								<li class="in"><marquee>
										<img class="avatar" alt=""
											src="assets/admin/layout/img/avatar1.jpg" />
										<div class="message">
											<span class="arrow"> </span> <a href="#" class="name">
												Bob Nilson </a> <span class="datetime"> at 20:09 </span> <span
												class="body"> Lorem ipsum dolor sit amet,
												consectetuer adipiscing elit, sed diam nonummy nibh euismod
												tincidunt ut laoreet dolore magna aliquam erat volutpat.</span>
										</div>

									</marquee></li>
							</ul>
							</p>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-12">
						<div class="note note-success">
							<h4>
								<b>Thought of the day...</b>
							</h4>
							<p>The quality of your work ,in the long run,is the deciding
								factor on how much your services are valued by the world.</p>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-6 col-sm-6">
						<!-- BEGIN PORTLET-->
						<div class="portlet box blue-madison">
							<div class="portlet-title line">
								<div class="caption">
									<i class="fa fa-gift"></i>Birthday's
								</div>
							</div>
							<div class="portlet-body" id="chats">
								<div class="scroller" style="height: 352px;"
									data-always-visible="1" data-rail-visible1="1">
									<ul class="chats">
									<%
									
									ResultSet rs=null,rs1=null;
									PreparedStatement ps=null,ps1=null;
									
									con=doConnection.getConnection();
									int cnt=0;

                                    String s =null,dept=null;
                                   
                                   // Format formatter1 = new SimpleDateFormat("yyyy");
                                    //String s1 = formatter1.format(date1);
                                   // System.out.println(s1);
									ps=con.prepareStatement("select NAME,DEPARTMENT,DATEBIRTH,DATEPART(DD,datebirth)as dayno,DATEPART(MM,datebirth) as MonthNo,* from EMPLOYEE_MASTER where DATEPART(MM,datebirth)= DATEPART(MM,GETDATE()) and DATEPART(DD,datebirth) >= DATEPART(DD,GETDATE()) order by dayno");
									rs=ps.executeQuery();
									while(rs.next())
									{
										cnt++;
									s=rs.getString(5);
									ps1=con.prepareStatement("select name from DEPT_MASTER where code=?");
									ps1.setString(1, rs.getString(2));
									rs1=ps1.executeQuery();
									while(rs1.next())
									{
										dept=rs1.getString(1);
									}
									
									
									if(cnt%2==0){
									%>
									
									
										<li class="out"><img class="avatar" alt=""
											src="assets/admin/layout/img/avatar1.jpg" />
											<div class="message">
												<span class="arrow"> </span> <a href="#" class="name">
												
												<%
												
                                                    String month=null;
                                                    if (s == "01") {
                                                        month = "JAN";
                                                    }else if(s.equals("02")){
                                                        month="FEB";
                                                    }else if(s.equals("03")){
                                                        month="MAR";
                                                    }else if(s.equals("04")){
                                                        month="APR";
                                                    }else if(s.equals("05")){
                                                        month="MAY";
                                                    }else if(s.equals("06")){
                                                        month="JUN";
                                                    }else if(s.equals("07")){
                                                        month="JUL";
                                                    }else if(s.equals("08")){
                                                        
                                                        month="AUG";
                                                    }else if(s.equals("09")){
                                                        month="SEP";
                                                    }else if(s.equals("10")){
                                                        month="OCT";
                                                    }else if(s.equals("11")){
                                                        month="NOV";
                                                    }else if(s.equals("12")){
                                                        month="DEC";
                                                    }
                                                        %>
													<%=rs.getString(1) %> </a> <span class="datetime"> on <strong><%=rs.getString(4) %>
														<%=month %></strong>
												</span> <span class="body"> <Strong> Department:</Strong> <%=dept%>
													<br> <a href="#">Wish him...</a></span>
											</div></li><%}else{
									%>
										<li class="in"><img class="avatar" alt=""
											src="assets/admin/layout/img/avatar1.jpg" />
											<div class="message">
												<span class="arrow"> </span> <a href="#" class="name">
												
												<%
												
                                                    String month=null;
                                                    if (s == "01") {
                                                        month = "JAN";
                                                    }else if(s.equals("02")){
                                                        month="FEB";
                                                    }else if(s.equals("03")){
                                                        month="MAR";
                                                    }else if(s.equals("04")){
                                                        month="APR";
                                                    }else if(s.equals("05")){
                                                        month="MAY";
                                                    }else if(s.equals("06")){
                                                        month="JUN";
                                                    }else if(s.equals("07")){
                                                        month="JUL";
                                                    }else if(s.equals("08")){
                                                        
                                                        month="AUG";
                                                    }else if(s.equals("09")){
                                                        month="SEP";
                                                    }else if(s.equals("10")){
                                                        month="OCT";
                                                    }else if(s.equals("11")){
                                                        month="NOV";
                                                    }else if(s.equals("12")){
                                                        month="DEC";
                                                    }
                                                        %>
													<%=rs.getString(1) %> </a> <span class="datetime"> on <strong><%=rs.getString(4) %>
														<%=month %></strong>
												</span> <span class="body"> <Strong> Department:</Strong> <%=dept%>
													<br> <a href="#">Wish him...</a></span>
											</div></li>
									<%}} %>	
									</ul>
								</div>
							</div>
						</div>
						<!-- END PORTLET-->
					</div>
					<div class="col-md-6 col-sm-6">
						<!-- BEGIN PORTLET-->
						<div class="portlet box blue-madison">
							<div class="portlet-title line">
								<div class="caption">
									<i class="icon-user-follow"></i>New Joinees
								</div>
							</div>
							<div class="portlet-body" id="chats">
								<div class="scroller" style="height: 352px;"
									data-always-visible="1" data-rail-visible1="1">
									<ul class="chats">
<%
									
									ResultSet rs3=null,rs4=null;
									PreparedStatement ps3=null,ps4=null;
									
									con=doConnection.getConnection();
									int cnt1=0;

                                    String s3 =null,dept1=null;
                                   
                                   // Format formatter1 = new SimpleDateFormat("yyyy");
                                    //String s1 = formatter1.format(date1);
                                   // System.out.println(s1);
									ps=con.prepareStatement("select NAME,DEPARTMENT,DATEBIRTH,DATEPART(DD,datejoin)as dayno,DATEPART(MM,datejoin) as MonthNo,DATEPART(yyyy,datejoin) as YearNo,city from EMPLOYEE_MASTER where DATEPART(MM,datejoin)= DATEPART(MM,GETDATE()) and DATEPART(yyyy,datejoin) = DATEPART(yyyy,GETDATE()) order by dayno");
									rs=ps.executeQuery();
									while(rs.next())
									{
										cnt++;
									s=rs.getString(5);
									ps1=con.prepareStatement("select name from DEPT_MASTER where code=?");
									ps1.setString(1, rs.getString(2));
									rs1=ps1.executeQuery();
									while(rs1.next())
									{
										dept=rs1.getString(1);
									}
									
									
									if(cnt%2==0){
									%>

										<li class="out"><img class="avatar" alt=""
											src="assets/admin/layout/img/avatar3.jpg" />
											<div class="message">
												<span class="arrow"> </span> <a href="#" class="name">
												<%
												
                                                    String month=null;
                                                    if (s == "01") {
                                                        month = "JAN";
                                                    }else if(s.equals("02")){
                                                        month="FEB";
                                                    }else if(s.equals("03")){
                                                        month="MAR";
                                                    }else if(s.equals("04")){
                                                        month="APR";
                                                    }else if(s.equals("05")){
                                                        month="MAY";
                                                    }else if(s.equals("06")){
                                                        month="JUN";
                                                    }else if(s.equals("07")){
                                                        month="JUL";
                                                    }else if(s.equals("08")){
                                                        
                                                        month="AUG";
                                                    }else if(s.equals("09")){
                                                        month="SEP";
                                                    }else if(s.equals("10")){
                                                        month="OCT";
                                                    }else if(s.equals("11")){
                                                        month="NOV";
                                                    }else if(s.equals("12")){
                                                        month="DEC";
                                                    }
                                                        %>
												
													<%=rs3.getString(1) %> </a> <span class="datetime"> Joined on <%=rs.getString(4)%>
													<%=month %> <%=rs.getString(6)%> </span> <span class="body"> <Strong>
														Department:</Strong> <%=dept1 %> <br> <Strong> About Me:</Strong> I am
													from <%=rs3.getString(7) %>.... <br> <a href="#">Know More...</a></span> </span>
											</div></li>
											<%}else{
												
											%>
										<li class="in"><img class="avatar" alt=""
											src="assets/admin/layout/img/avatar3.jpg" />
											<div class="message">
												<span class="arrow"> </span> <a href="#" class="name">
												<%
												
                                                    String month=null;
                                                    if (s == "01") {
                                                        month = "JAN";
                                                    }else if(s.equals("02")){
                                                        month="FEB";
                                                    }else if(s.equals("03")){
                                                        month="MAR";
                                                    }else if(s.equals("04")){
                                                        month="APR";
                                                    }else if(s.equals("05")){
                                                        month="MAY";
                                                    }else if(s.equals("06")){
                                                        month="JUN";
                                                    }else if(s.equals("07")){
                                                        month="JUL";
                                                    }else if(s.equals("08")){
                                                        
                                                        month="AUG";
                                                    }else if(s.equals("09")){
                                                        month="SEP";
                                                    }else if(s.equals("10")){
                                                        month="OCT";
                                                    }else if(s.equals("11")){
                                                        month="NOV";
                                                    }else if(s.equals("12")){
                                                        month="DEC";
                                                    }
                                                        %>
												
													<%=rs3.getString(1) %> </a> <span class="datetime"> Joined on <%=rs.getString(4)%>
													<%=month %> <%=rs.getString(6)%> </span> <span class="body"> <Strong>
														Department:</Strong> <%=dept1 %> <br> <Strong> About Me:</Strong> I am
													from <%=rs3.getString(7) %>.... <br> <a href="#">Know More...</a></span> </span>
											</div></li>											
											
											
											
											<%}
									
									} %>
										
									</ul>
								</div>
							</div>
						</div>
						<!-- END PORTLET-->
					</div>



					<!-- END PAGE CONTENT-->
				</div>
			</div>
			<!-- END CONTENT -->
			<!-- BEGIN QUICK SIDEBAR -->
			<jsp:include page="quicksidebar.jsp"></jsp:include>
			<!-- END QUICK SIDEBAR -->
		</div>
		<!-- END CONTAINER -->
		<!-- BEGIN FOOTER -->

		<jsp:include page="footer.jsp"></jsp:include>
		<!-- END FOOTER -->

		<jsp:include page="footerscripts.jsp"></jsp:include>
</body>
<!-- END BODY -->
</html>